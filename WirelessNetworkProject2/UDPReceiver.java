import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;


public class UDPReceiver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		while(true)
		{
			try
			{
				byte[] buffer = new byte[65000];
				DatagramPacket incoming = new DatagramPacket(buffer,buffer.length);
				DatagramSocket ds = new DatagramSocket(60000);
				ds.receive(incoming);
				byte[] data = incoming.getData();
				String s = new String(data,0,data.length);
				System.out.println("Port: "+incoming.getPort()+" on: "+incoming.getAddress());
				System.out.println(s);
						
			}
			catch(IOException e)
			{
				System.err.println(e);
			}
		}

	}

}
