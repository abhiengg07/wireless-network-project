
public class IPandPortObject {

	/**
	 * @param args
	 */
	int sendingPort;
	int receivingPort;
	byte[] ipAddress;
	static IPandPortObject instance = null;
	boolean running;
	private IPandPortObject()
	{
		

	}
	
	public static IPandPortObject getInstance()
	{
		if(instance == null)
		{
			instance = new IPandPortObject();
			return instance;
		}
		else
		{
			return instance;
		}
	}
	
	public int getSendingPort()
	{
		return sendingPort;
	}
	
	public int getReceivingPort()
	{
		return receivingPort;
	}
	
	public byte[] getIPAddress()
	{
		return ipAddress;
	}
	
	public boolean getIsRunning()
	{
		return running;
	}
	
	public void setSendingPort(int newSendingPort)
	{
		sendingPort = newSendingPort;
	}
	
	public void setReceivingPort(int newReceivingPort)
	{
		receivingPort = newReceivingPort;
	}
	
	public void setIPAddress(byte[] newIpAddress)
	{
		ipAddress = newIpAddress;
	}
	
	public void setIsRunning(boolean newRunning)
	{
		running = newRunning;
	}

}
