import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;


public class RecordAndSendThread implements Runnable {

	@Override
	public void run() {
		//set up computer connection
		IPandPortObject i = IPandPortObject.getInstance();
		byte[] address = i.getIPAddress();
		int port = i.getSendingPort();
		DatagramPacket dp = null;
		DatagramSocket sender = null;
		InetAddress ia = null;
		try {
			ia = InetAddress.getByAddress(address);
		} catch (UnknownHostException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			sender = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		
		
		//set up microphone
		TargetDataLine line = null;
		float samplesPerSecond = 8000;
		int sampleSizeInBits = 8;
		int numberOfChannels = 1;
		boolean signed = true;
		boolean bigEndian = true;
		AudioFormat format = new AudioFormat(samplesPerSecond, sampleSizeInBits, numberOfChannels, signed, bigEndian);
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        try 
        {
			line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(format);
        } 
        catch (LineUnavailableException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
        line.start();
        
      
        
        
        //record microphone information
        
        int bufferSize = (int)format.getSampleRate() * format.getFrameSize();
        System.out.println("BufferSize: "+bufferSize);
        byte buffer[] = new byte[bufferSize];
        System.out.println("Recording");
        while(i.getIsRunning())
        {
        	int count = line.read(buffer, 0, buffer.length);
        	if(count > 0)
        	{
        		dp = new DatagramPacket(buffer,buffer.length,ia,port);
        		try {
					sender.send(dp);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        }
        sender.close();
        sender.disconnect();
	}

}
