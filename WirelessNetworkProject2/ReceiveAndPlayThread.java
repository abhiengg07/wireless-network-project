import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;


public class ReceiveAndPlayThread implements Runnable {

	@Override
	public void run() {
		DatagramSocket ds = null;
		IPandPortObject i = IPandPortObject.getInstance();
		try {
			ds = new DatagramSocket(i.getReceivingPort());
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		float samplesPerSecond = 8000;
		int sampleSizeInBits = 8;
		int numberOfChannels = 1;
		boolean signed = true;
		boolean bigEndian = true;
		AudioFormat format = new AudioFormat(samplesPerSecond, sampleSizeInBits, numberOfChannels, signed, bigEndian);
        DataLine.Info info = new DataLine.Info(SourceDataLine.class,format);
        SourceDataLine sLine = null;
        try 
        {
			sLine = (SourceDataLine)AudioSystem.getLine(info);
		    sLine.open(format);
        } 
        catch (LineUnavailableException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        sLine.start();
        
        
        //playback audio
        int bufferSize = (int) format.getSampleRate() * format.getFrameSize();
        byte buffer[] = new byte[bufferSize];
        DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);
        try {
			while(i.getIsRunning())
			{
				ds.receive(incoming);
				byte[] data = incoming.getData();
			    sLine.write(data,0,data.length);
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        sLine.drain();
        sLine.close();
        ds.close();
        ds.disconnect();

	}

}
