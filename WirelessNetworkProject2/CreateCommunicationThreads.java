import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;


public class CreateCommunicationThreads {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		buildGUI();


	}

	public static void buildGUI()
	{
		JFrame frame = new JFrame("Network Communicator");
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.PAGE_AXIS));
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		Container contentPane = frame.getContentPane();
		
		//contentPane.setLayout(new SpringLayout());
		JLabel ipLabel = new JLabel("IP Address:",JLabel.TRAILING);;
		JLabel sendingPortLabel = new JLabel("Port to Send To:",JLabel.TRAILING);
		JLabel receivingPortLabel = new JLabel("Receiving Port:",JLabel.TRAILING);
		final JTextField ipText = new JTextField(20);
		final JTextField sendingPortText = new JTextField(20);
		final JTextField receivingPortText = new JTextField(20);
		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);
		final JButton startButton = new JButton("Start");
		startButton.addMouseListener(new MouseAdapter()
		{
			@Override
            public void mouseClicked(MouseEvent e) {
				if(startButton.getText().equals("Start"))
				{
				  startButton.setText("Stop");
				  IPandPortObject i = IPandPortObject.getInstance();
				  i.setIsRunning(true);
				  String ipString = ipText.getText();
				  String sendingPortString = sendingPortText.getText();
				  String receivingPortString = receivingPortText.getText();
				  byte[] b = new byte[4];
				  int pos = ipString.indexOf(".");
				  for(int j = 0; j < 3; j++)
			  	  {
				    if(pos == -1)
				    {
				    	JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
                            JOptionPane.ERROR_MESSAGE);
				  	  return;
				    }
				    else
				    {
					  String number = ipString.substring(0,pos);
					  int num;
					  try{
					    num = Integer.parseInt(number);
				 	  }
					  catch(Exception ex)
					  {
						  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
	                            JOptionPane.ERROR_MESSAGE);
						  return;
					  }
					  if(num > 255 || num < 0)
					  {
						  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
	                            JOptionPane.ERROR_MESSAGE);
						  return;
					  }
					  b[j] = (byte)num;
					  ipString = ipString.substring(pos+1);
					  pos = ipString.indexOf(".");
				    }
				  }
				  int num;
				  try{
				    num = Integer.parseInt(ipString);
				  }
				  catch(Exception ex)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  if(num > 255 || num < 0)
				  {
				 	  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  b[3] = (byte)num;
				  num = -99;
				  try{
					  num = Integer.parseInt(sendingPortString);
				  }
				  catch(Exception ex)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid sending port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  
				  i.setIPAddress(b);
				  if(num > 65535 || num  < 1)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid sending port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  else
				  {
					  i.setSendingPort(num);
				  }
				
				  num = -99;
				  try{
					  num = Integer.parseInt(receivingPortString);
				  }
				  catch(Exception ex)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid receiving port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  if(num > 65535 || num  < 1)
				  {
				  	  JOptionPane.showMessageDialog(null, "Please input a valid receiving port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  else
				  {
					  i.setReceivingPort(num);
				  }
				
				  Thread thread1 = new Thread(new ReceiveAndPlayThread());
				  Thread thread2 = new Thread(new RecordAndSendThread());
				  thread1.start();
				  thread2.start();
				}
				else
				{
					startButton.setText("Start");
					IPandPortObject i = IPandPortObject.getInstance();
					i.setIsRunning(false);
				}
			}
		});
		mainPanel.add(startButton);
		panel1.add(ipLabel);
		ipLabel.setLabelFor(ipText);
		panel1.add(ipText);
		panel2.add(sendingPortLabel);
		sendingPortLabel.setLabelFor(sendingPortText);
		panel2.add(sendingPortText);
		panel3.add(receivingPortLabel);
		receivingPortLabel.setLabelFor(receivingPortText);
		panel3.add(receivingPortText);
		contentPane.add(mainPanel);
		frame.pack();
		frame.setSize(400,200);
		frame.setVisible(true);
		
	}
	

}
