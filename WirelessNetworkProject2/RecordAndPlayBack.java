import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;


public class RecordAndPlayBack {
	static boolean running;
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		//open line to microphone
		TargetDataLine line = null;
		float samplesPerSecond = 8000;
		int sampleSizeInBits = 8;
		int numberOfChannels = 1;
		boolean signed = true;
		boolean bigEndian = true;
		AudioFormat format = new AudioFormat(samplesPerSecond, sampleSizeInBits, numberOfChannels, signed, bigEndian);
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        try 
        {
			line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(format);
        } 
        catch (LineUnavailableException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
        line.start();
        
        
        
        //record microphone information
        Timer t = new Timer();
        t.schedule(new TimerTask() {
			
			@Override
			public void run() {
				running = false;
			}
		}, 10 * 1000);
        
        running = true;
        int bufferSize = (int)format.getSampleRate() * format.getFrameSize();
        System.out.println("BufferSize: "+bufferSize);
        byte buffer[] = new byte[bufferSize];
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.out.println("Recording");
        while (running)
        {
        	int count = line.read(buffer, 0, buffer.length);
        	if(count > 0)
        	{
        		out.write(buffer,0,count);
        	}
        }
        System.out.println("finished Recording");
        try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        //converting output to input
        byte audio[] = out.toByteArray();
        InputStream input = new ByteArrayInputStream(audio);
        AudioInputStream ais = new AudioInputStream(input,format,audio.length/format.getFrameSize());
        info = new DataLine.Info(SourceDataLine.class,format);
        SourceDataLine sLine = null;
        try 
        {
			sLine = (SourceDataLine)AudioSystem.getLine(info);
		    sLine.open(format);
        } 
        catch (LineUnavailableException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        sLine.start();
        
        
        //playback audio
        int bufferSize2 = (int) format.getSampleRate() * format.getFrameSize();
        byte buffer2[] = new byte[bufferSize2];
        
        int count;
        try {
			while((count = ais.read(buffer2,0,buffer2.length)) != -1)
			{
				if (count > 0)
				{
					sLine.write(buffer2,0,count);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        sLine.drain();
        sLine.close();
        t.cancel();
	}

}
