// PHANI SEKHAR NIMMALA
// ABHISHEK KULKARNI
// ZACK COKER


// class IPandPortObject used in the GUI in the CreateCommunicationThreads class and other 

public class IPandPortObject {


//Declaring the variables
	int sendingPort;
	int receivingPort;
	byte[] ipAddress;
	static IPandPortObject instance = null;
	boolean running;
	private IPandPortObject()
	{
		

	}

//This method creates a new instance if already exists returns that
	
	public static IPandPortObject getInstance()
	{
		if(instance == null)
		{
			instance = new IPandPortObject();
			return instance;
		}
		else
		{
			return instance;
		}
	}
	
//Method used to return the sending port number

	public int getSendingPort()
	{
		return sendingPort;
	}

//Method used to return the receiving port number
	
	public int getReceivingPort()
	{
		return receivingPort;
	}

//Method used to return the IP address
	
	public byte[] getIPAddress()
	{
		return ipAddress;
	}

//Method used to return the status Running
	
	public boolean getIsRunning()
	{
		return running;
	}

//Method used to create new sending port
	
	public void setSendingPort(int newSendingPort)
	{
		sendingPort = newSendingPort;
	}

//Method used to create a new receiving port
	
	public void setReceivingPort(int newReceivingPort)
	{
		receivingPort = newReceivingPort;
	}

//Method used to create a new IP address

	public void setIPAddress(byte[] newIpAddress)
	{
		ipAddress = newIpAddress;
	}
	
//Method changes the status from true to false and from false to true
	
        public void setIsRunning(boolean newRunning)
	{
		running = newRunning;
	}

}
