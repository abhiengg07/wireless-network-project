// PHANI SEKHAR NIMMALA
// ABHISHEK KULKARNI
// ZACK COKER


// importing the required libraries

import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;

//Name:CreateCommunicationThreads 
//this class creates a GUI and starts two threads

        public class CreateCommunicationThreads {

//Main method calls buildGUI

	public static void main(String[] args) 
	{
		buildGUI();


	}

//This method builds the GUI
        
	public static void buildGUI()
	{

//Creating a new frame

 
		JFrame frame = new JFrame("Network Communicator");

//Creating a main panel in the frame with a box layout
	
		JPanel mainPanel = new JPanel();
                mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.PAGE_AXIS));

//Three new panels	
	
                JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		Container contentPane = frame.getContentPane();
		

//labeling the 3 panels

		JLabel ipLabel = new JLabel("IP Address:",JLabel.TRAILING);;
		JLabel sendingPortLabel = new JLabel("Port to Send To:",JLabel.TRAILING);
		JLabel receivingPortLabel = new JLabel("Receiving Port:",JLabel.TRAILING);

//Text field is 20 Characters
		
                final JTextField ipText = new JTextField(20);
		final JTextField sendingPortText = new JTextField(20);
		final JTextField receivingPortText = new JTextField(20);

//The 3 panels are added to the main panel
		
 		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);

//Start button is created 
		
final JButton startButton = new JButton("Start");

//Using MouseAdapter so that we can neednot override all the methods of the interface	
	
startButton.addMouseListener(new MouseAdapter()
		{
			@Override //overriding the mouseclicked method
            public void mouseClicked(MouseEvent e) {

//When the start button is clicked, change the text to stop and take the information from the text feilds
			
                                if(startButton.getText().equals("Start"))
				{
				  startButton.setText("Stop");
				  IPandPortObject i = IPandPortObject.getInstance();
				  i.setIsRunning(true);
				  String ipString = ipText.getText();
				  String sendingPortString = sendingPortText.getText();
				  String receivingPortString = receivingPortText.getText();
				  byte[] b = new byte[4];
				  int pos = ipString.indexOf(".");

//Error check for the correct syntax of IP address

//Use of for loop to check  for the first three parts of IP address 

				  for(int j = 0; j < 3; j++)
			  	  {
				    if(pos == -1)
				    {

//A new window pops up displaying message in case of error

				    	JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
                            JOptionPane.ERROR_MESSAGE);
				  	  return;
				    }
				    else
				    {

					  String number = ipString.substring(0,pos);
					  int num;
					  try{
					    num = Integer.parseInt(number);
				 	  }
					  catch(Exception ex)
					  {
						  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
	                            JOptionPane.ERROR_MESSAGE);
						  return;
					  }
					  if(num > 255 || num < 0)
					  {
						  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
	                            JOptionPane.ERROR_MESSAGE);
						  return;
					  }
					  b[j] = (byte)num;
					  ipString = ipString.substring(pos+1);
					  pos = ipString.indexOf(".");
				    }
				  }
//The below statements are used to check the last part of the IP address

				  int num;
				  try{
				    num = Integer.parseInt(ipString);
				  }
				  catch(Exception ex)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  if(num > 255 || num < 0)
				  {
				 	  JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
                                   }
                                   b[3] = (byte)num;

//Error check for the sending port number
				 
				  
				  num = -99;
				  try{
					  num = Integer.parseInt(sendingPortString);
				  }
				  catch(Exception ex)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid sending port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  
				  i.setIPAddress(b);
				  if(num > 65535 || num  < 1)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid sending port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  else
				  {
					  i.setSendingPort(num);
				  }

//Error check for the receiving port number				


				  num = -99;
				  try{
					  num = Integer.parseInt(receivingPortString);
				  }
				  catch(Exception ex)
				  {
					  JOptionPane.showMessageDialog(null, "Please input a valid receiving port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  if(num > 65535 || num  < 1)
				  {
				  	  JOptionPane.showMessageDialog(null, "Please input a valid receiving port number.", "Error",
                            JOptionPane.ERROR_MESSAGE);
					  return;
				  }
				  else
				  {
					  i.setReceivingPort(num);
				  }

// Start 2 threads for the audio message to be received and play , and to record and send it.

				  Thread thread1 = new Thread(new ReceiveAndPlayThread());
				  Thread thread2 = new Thread(new RecordAndSendThread());
				  thread1.start();
				  thread2.start();
				}
				else
				{
					startButton.setText("Start");
					IPandPortObject i = IPandPortObject.getInstance();
					i.setIsRunning(false);
				}
			}
		});

//The labels and textfeilds are added to the subpanels

		mainPanel.add(startButton);
		panel1.add(ipLabel);
		ipLabel.setLabelFor(ipText);
		panel1.add(ipText);
		panel2.add(sendingPortLabel);
		sendingPortLabel.setLabelFor(sendingPortText);
		panel2.add(sendingPortText);
		panel3.add(receivingPortLabel);
		receivingPortLabel.setLabelFor(receivingPortText);
		panel3.add(receivingPortText);
		contentPane.add(mainPanel);
		frame.pack();

//Sets the starting frame size and displays the GUI

		frame.setSize(400,200);
		frame.setVisible(true);
		
	}
	

}
