package com.wireless.main;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/*
 *  The class used to sound information between the threads.
 *  Used to communicate between the receive and send and
 *  the play thread. 
 */
public class SoundInformationLock {


	// the singleton instance of the class
	static SoundInformationLock instance = null;
	
	Queue<Packet> packetQueue = new ConcurrentLinkedQueue<Packet>();
	
	/*
	 * the default constructor
	 */
	private SoundInformationLock() 
	{
		
	}
	
	/*
	 * returns an instance of the class
	 */
	public static SoundInformationLock getInstance()
	{
		if(instance == null)
		{
			instance = new SoundInformationLock();
		}
		return instance;
	}
	
	/*
	 * sets the information stored to the new packet
	 * 
	 * newPacket - the packet to store
	 */
	public synchronized void setInformation(Packet newPacket)
	{
		//System.out.println("Packet added to queue: "+newPacket.getSequenceNumber());
		packetQueue.add(newPacket);
	}

	/*
	 * returns the information stored or null otherwise.
	 */
	public synchronized Packet getInformation()
	{
	  Packet packet;	
	  if(packetQueue.peek() != null)
	  {
		  packet = packetQueue.remove();
	  }
	  else
	  {
		  packet = null;
	  }
	  //Packet packet = packetQueue.poll();
	  if(packet != null)
	  {
	    //System.out.println("Packet out of queue: "+packet.getSequenceNumber()+" new queue size: "+packetQueue.size());
	  }
	  return packet;
	}
}
