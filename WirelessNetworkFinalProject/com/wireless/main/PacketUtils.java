package com.wireless.main;

public class PacketUtils
{
  /*
   * copies the byte array source into the packet
   * 
   * source - the byte array to copy into the packet.
   */
  public static int copyIntoPacket(byte[] source, int messagePointer, byte[] packet)
  {
    for(int i = 0; i < source.length; i++)
    {
      packet[messagePointer] = source[i];
      messagePointer++;
    }
    return messagePointer;
  }
  
  /*
   * converts an integer value to a byte array
   */
  public static byte[] intToByteArray(int value, int size) 
  {
    byte[] b = new byte[size]; 
    for(int i = 0; i < size; i++)
    {
      b[i] = (byte)(value >> (8 * i));  
    }
    return b;
  }
	
  /*
   * Converts byte arrays to ints
   * 
   */
  public static int byteArrayToInt(byte[] b)
  {
    int result = 0;
    for(int i = 0; i < b.length; i++)
    {
      result = result | ((b[i] << (8*i)) & (0x000000FF << (8* i))); 
    }
  return result;
  }
}
