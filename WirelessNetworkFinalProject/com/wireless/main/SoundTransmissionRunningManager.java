package com.wireless.main;

import java.util.HashMap;

/*
 * The class that manages which threads are running
 */
public class SoundTransmissionRunningManager 
{

	// the singleton instance of the class
	static SoundTransmissionRunningManager instance = null;
	//the hash map between the thread id and their status
	HashMap<Integer,Boolean> runningMap = new HashMap<Integer,Boolean>();
	
   /*
    * The default constructor for the class
    */
	private SoundTransmissionRunningManager() 
	{
		
	}
	
	/* 
	 * Returns an instance of the class
	 */
	public static SoundTransmissionRunningManager getInstance()
	{
	  if(instance == null)
	  {
		  instance = new SoundTransmissionRunningManager();
	  }
	  return instance;
	}
	
	/*
	 * adds a thread to the manager
	 * 
	 * nodeNubmer - the node for the thread
	 * runningStatus - the running status of the thread
	 */
	public void addThread(int nodeNumber, boolean runningStatus)
	{
		if(!runningMap.containsKey(new Integer(nodeNumber)))
		{
			runningMap.put(new Integer(nodeNumber), new Boolean(runningStatus));
		}
	}
	
	/*
	 * removes the thread from the thread manager
	 * 
	 * nodeNubmer - the node for the thread to remove
	 */
	public void removeThread(int nodeNumber)
	{
		if(runningMap.containsKey(new Integer(nodeNumber)))
		{
		  runningMap.remove(new Integer(nodeNumber));
		}
	}
	
    /*
     * returns if the thread is running for the given node
     * true if the node is running, false if it is not
     * 
     * nodeNumber - the node for the thread to check
     */
	public boolean isThreadRunning(int nodeNumber)
	{
		if(runningMap.containsKey(new Integer(nodeNumber)))
		{
			return runningMap.get(new Integer(nodeNumber));
		}
		else
		{
			return false;
		}
	}
	
	/*
	 * Sets the running status of a previously created thread
	 * 
	 * nodeNubmer - the node number for the thread
	 * runningStatus - the new running status of the thread.
	 */
	public void setThreadRunningStatus(int nodeNumber, boolean runningStatus)
	{
		if(runningMap.containsKey(new Integer(nodeNumber)))
		{
			runningMap.put(new Integer(nodeNumber), new Boolean(runningStatus));
		}
	}

}
