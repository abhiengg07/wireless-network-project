package com.wireless.main;

import java.util.Stack;

/*
 * The stack of the currently running threads
 */
public class ThreadIDStack 
{
    // the singleton instance of the class
	static ThreadIDStack instance = null;
	// the stack used to store the thread IDS
	Stack<Integer> ids = new Stack<Integer>();
	
	/*
	 * The default constructor of the class
	 */
	private ThreadIDStack()
	{
		
	}

	/*
	 * Gets the instance of the singleton object
	 */
	public static ThreadIDStack getInstance()
	{
		if(instance == null)
		{
			instance = new ThreadIDStack();
		}
		return instance;
	}
	
	/* 
	 * takes the id off of the top of the stack and returns it
	 */
	public int pop()
	{
		return ids.pop().intValue();
	}
	
	/*
	 * Adds a new id to the top of the stack
	 * 
	 * newId - the id to add
	 */
	public void push(int newId)
	{
		ids.push(new Integer(newId));
	}
	
	/*
	 * Checks if the stack is empty
	 * 
	 * returns true if the stack is empty and false otherwise
	 */
	public boolean isEmpty()
	{
		return ids.isEmpty();
	}
	
	
	

}
