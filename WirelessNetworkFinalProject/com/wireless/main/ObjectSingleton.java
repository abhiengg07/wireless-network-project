package com.wireless.main;

import java.util.HashMap;

/*
 * The class used to get global variables.
 */
public class ObjectSingleton {

	
    // the single instance of the object
	private static ObjectSingleton instance = null;
	//  The Network Configuration object for the program 
	NetworkConfiguration nc;
	//The filename for the network configuration
	String filename;
	// the variable maintaining if the network configuration thread
	// is running
	boolean isConfigurationThreadRunning = false;
	// the name of the computer system.
	String computerName;
	// the hash map for the flooding protocol managers; used to get the same configuration for 
	// the recording threads.  May be expanded on later.
	HashMap<Integer,FloodingProtocolManager> fpmHash = new HashMap<Integer,FloodingProtocolManager>();
	
	boolean isPlaying = false;
	
	boolean isRecording = false;
	
	TransmissionWindow tw;
	
	public TransmissionWindow getTansmissionWindow()
	{
	  return tw;
	}

	public void setTransmissionWindow(TransmissionWindow tw)
	{
	  this.tw = tw;
	}

	/*
	 * the default constructor for the class.
	 */
	private ObjectSingleton() {
		
	}
	
	/*
	 * Returns the instance of the ObjectSingleton
	 * for the singleton class
	 */
	public static ObjectSingleton getInstance()
	{
		if(instance == null)
		{
			instance = new ObjectSingleton();
			return instance;
		}
		else
		{
			return instance;
		}
	}
	
	/*
	 * Returns the network configuration for the program
	 */
	public NetworkConfiguration getNetworkConfiguration()
	{
		return nc;
	}
	
	/*
	 * Sets the network configuration
	 * 
	 * newNC - the new network configuration
	 */
	public void setNetworkConfiguration(NetworkConfiguration newNC)
	{
		nc = newNC;
	}

	/*
	 * returns the filename of the configuration file
	 */
	public String getConfigurationFilename() 
	{
		return filename;
	}
	
	/*
	 * sets the filename of the configuration file
	 * 
	 * newFilname - the new file name of the configuration file
	 */
	public void setConfigurationFilename(String newFilename)
	{
		filename = newFilename;
	}

	/*
	 * Returns the status of the configuration thread.
	 * 
	 * true if it is running; false otherwise.
	 */
	public boolean getIsConfigurationRunning()
	{
		return isConfigurationThreadRunning;
	}
	
	/*
	 * Sets the running status
	 * 
	 * newRunningStatus - if the configuration thread is running
	 */
	public void setIsConfigurationRunning(boolean newRunningStatus)
	{
	   isConfigurationThreadRunning = newRunningStatus;
	}
	
	/*
	 * returns the computer name
	 */
	public String getComputerName()
	{
		return computerName;
	}
	
	/*
	 * sets the computer name
	 * 
	 * newComputerName - the new computer name
	 */
	public void setComputerName(String newComputerName)
	{
		computerName = newComputerName;
	}
	
	/*
	 * The method to add a flooding protocol manager to the hash
	 * 
	 * node - the number of the node the manager is for
	 * fpm - the flooding protocol manager 
	 */
	public void addToFPMHash(int node, FloodingProtocolManager fpm)
	{
		if(!fpmHash.containsKey(new Integer(node)))
		{
			fpmHash.put(new Integer(node), fpm);
		}
	}
	
	/*
	 *  Returns the flooding protocol manager that corresponds
	 *  to the given node
	 *  
	 *  node - the node corresponding to the manager
	 */
	public FloodingProtocolManager getFPMfromHash(int node)
	{
		return fpmHash.get(new Integer(node));
	}
	
	/*
	 * Removes the current node's manager from the hash
	 * 
	 * node - the node corresponding to the manager
	 */
	public void removeFromFPMHash(int node)
	{
		if(fpmHash.containsKey(new Integer(node)))
		{
			fpmHash.remove(new Integer(node));
		}
	}
	
    public boolean getIsPlaying()
    {
    	return isPlaying;
    }
    
    public void setIsPlaying(boolean newPlayingStatus)
    {
    	isPlaying = newPlayingStatus;
    }
    
    public boolean getIsRecording()
    {
    	return isRecording;
    }
    
    public void setIsRecording(boolean newRecordingStatus)
    {
    	isRecording = newRecordingStatus;
    }
}
