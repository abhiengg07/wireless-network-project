package com.wireless.main;

/*
 * The exception raised when something with an invalid length
 * arrives as a parameter.
 */
public class LengthException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LengthException() {
		// TODO Auto-generated constructor stub
	}

	public LengthException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public LengthException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public LengthException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/*public LengthException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		//super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}
	*/

}