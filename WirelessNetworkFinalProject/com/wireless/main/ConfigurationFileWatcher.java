package com.wireless.main;

import java.io.IOException;
import java.io.File;
import java.util.ArrayList;


/*
 * This class watches the configuration file and notifies
 * the program if an update occurs to the file.
 * 
 * May have an error with the loop running while changing its running
 * status.  Will have to investigate it later or stop the thread in a
 * way similar to the way the other threads are stopped.
 */
public class ConfigurationFileWatcher implements Runnable {

	//the boolean that maintains if the thread is running
	boolean isRunning;
	//the time the file was last modified
	long lastModifiedTime;
	/*
	 * The default constructor for the class
	 */
	public ConfigurationFileWatcher() 
	{

	}

	/*
	 * Sets the boolean variable to running.
	 * newIsRunning - the boolean to set the status to.
	 */
	public void setIsRunning(boolean newIsRunning)
	{
		isRunning = newIsRunning;
	}
	
	/*
	 * Returns the running status of the thread
	 */
	public boolean getIsRunning()
	{
		return isRunning;
	}
	
	/*
	 *  Creates a file watcher on the specified file system and
	 *  then starts a loop to continually check the directory for 
	 *  changes to the configuration file.  When a change occurs, 
	 *  the network configuration is updated.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() 
	{
		NetworkConfiguration nc = ObjectSingleton.getInstance().getNetworkConfiguration();
		File file = new File(ObjectSingleton.getInstance().getConfigurationFilename());
		lastModifiedTime = file.lastModified();
		while(getIsRunning())
		{
           /* for (WatchEvent<?> event: key.pollEvents()) 
            {
                WatchEvent.Kind kind = event.kind();
                System.out.println("Context: "+ event.context());
                System.out.println(" testNumber: " + testNumber);
                testNumber++;
            }*/
		  if(lastModifiedTime != file.lastModified())
		  {
			lastModifiedTime = file.lastModified();  
            try 
      	    {
      		  nc.readConfigurationFile(ObjectSingleton.getInstance().getConfigurationFilename());
      	    }
      	    catch (IOException e) 
      	    {
      		  // TODO Auto-generated catch block
      	  	  e.printStackTrace();
      	    }
            ArrayList<NetworkNode> nodes = nc.getNodes();
            for(int i = 0; i < nodes.size(); i++)
            {
            	System.out.println("Size: "+nodes.size());
            	System.out.println(nodes.get(i).toString());
            }
            System.out.println("Out of loop."); 
		  }
          
            
		}

	}

}
