package com.wireless.main;

import java.io.Serializable;
import java.util.ArrayList;

import com.wireless.mobility.MobileArea;

/*
 * The class that contains the information for a node in 
 * a network.
 */
public class NetworkNode implements Serializable
{

	/**
	 *  The value used to see if the class is created correctly
	 *  when it is loaded after being saved.
	 */
	private static final long serialVersionUID = 800L;
	// the number of the node
	int number;
	// the name of the computer for the node.  In a real network, 
	// there would be one name for one node.  On a simulated environment,
	// there may be more.
	String name;
	// the node port
	int port;
	// the location of the port in the x direction
	double xCoordinate;
	// the location of the port in the y direction
	double yCoordinate;
	// the numbers of the nodes that are linked to the 
	// current node
	ArrayList<Integer> links = new ArrayList<Integer>();
	// the IP address of the node
	byte[] ipAdress;
	
	int direction;
        double velocity;
	MobileArea area;
	int nodeNumber;
	
	public int getDirection()
	{
	  return direction;
	}

	public void setDirection(int direction)
	{
	  direction = direction % 360;
	  if(direction < 0)
	  {
	    direction = 360 - direction;
	  }
	  this.direction = direction;
	}

	public double getVelocity()
	{
	  return velocity;
	}

	public void setVelocity(double velocity)
	{
	  this.velocity = velocity;
	}

	public MobileArea getArea()
	{
	  return area;
	}

	public void setArea(MobileArea area)
	{
	  this.area = area;
	}

	
	
	/*
	 * The default constructor of the class
	 */
	public NetworkNode()
	{
		
	}
	
	/*
	 * The constructor of the network nodes that sets the necessary attributes
	 * 
	 * newNumber - the number of the node
	 * newName - the name of the computer the node is on
	 * newIPAddress - the IP address for the computer of the node
	 * newPort - the port number on the computer for the node
	 * newXCoordinate - the x location of the node
	 * newYCoordinate - the y location of the node
	 * newLinks - the node numbers of the nodes that this node
	 *    has connection to.
	 */
	public NetworkNode(int newNumber, String newName, byte[] newIPAddress,
			int newPort, double newXCoordinate, double newYCoordinate, ArrayList<Integer> newLinks) 
	{
		number = newNumber;
		name = newName;
		ipAdress = newIPAddress;
		port = newPort;
		xCoordinate = newXCoordinate;
		yCoordinate = newYCoordinate;
		links = newLinks;
	}
	
	/*
	 * Returns the links that the node has connections to
	 */
	public ArrayList<Integer> getLinks()
	{
		return links;
	}
	
	/*
	 * Adds a link to the link list.
	 * 
	 * newLink - the node number of the newly linked node
	 */
	public void createLink(int newLink)
	{
		links.add(newLink);
	}
	
	/*
	 * Removes a link from the link list
	 * Throws a runtime exception if a node is removed that is not
	 * in the list
	 * 
	 * removeLink - the node number of the newly unlinked node
	 */
	public void removeLink(int removedLink)
	{
		if(links.contains(removedLink))
		{
			links.remove(removedLink);
		}
		else
		{
			throw new RuntimeException();
		}
	}
	
	/*
	 * Returns the name of the computer that the node is on.
	 */
	public String getName()
	{
		return name;
	}
	
	/*
	 * Returns the node number
	 */
	public int getNumber()
	{
		return number;
	}
	
	/*
	 * returns the IP address of the node's computer
	 */
	public byte[] getIPAddress()
	{
		return ipAdress;
	}
	
	/*
	 * returns the node's port on the computer
	 */
	public int getPort()
	{
		return port;
	}
	
	/*
	 * returns the xcoordinate of the node
	 */
	public double getXCoordinate()
	{
		return xCoordinate;
	}
	
	/*
	 * returns the ycoordinate of the node
	 */
	public double getYCoordinate()
	{
		return yCoordinate;
	}
	
	/*
	 * sets the name of the node
	 * 
	 * newName - the name to replace the old name with
	 */
	public void setName(String newName)
	{
		name = newName;
	}
	
	/*
	 * sets the node number
	 * 
	 * newNumber - the new node number
	 */
	public void setNumber(int newNumber)
	{
		number = newNumber;
	}
	
	/*
	 * sets the node's IP address
	 * 
	 * newIPAddress - the new IP address
	 */
	public void setIPAdress(byte[] newIPAdress)
	{
		ipAdress = newIPAdress;
	}
	
	/*
	 * sets the port number of the node
	 * 
	 * newPort - the port of the node
	 */
	public void setPort(int newPort)
	{
		port = newPort;
	}
	
	/*
	 * sets the x coordinate of the node
	 * 
	 * newXCoordinate - the new x coordinate for the node
	 */
	public void setXCoordinate(double newXCoordinate)
	{
		xCoordinate = newXCoordinate;
	}
	
	/*
	 * sets the y coordinate of the node
	 * 
	 * newYCoordinate - the new y coordiante for the node
	 */
	public void setYCoordinate(double newYCoordinate)
	{
		yCoordinate = newYCoordinate;
	}
	
	/*
	 * replaces the ArrayList for the links of the list
	 * 
	 * newLinks - the new list of links 
	 */
	public void setLinks(ArrayList<Integer> newLinks)
	{
		links = newLinks;
	}
	
	/*
	 * The method used to print out nodes.
	 * 
	 * Right now it is not printing out the IP address.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String result = "Node: "+getNumber()+"\n";
		result += "Name: "+getName()+"\n";
		result += "Port: "+getPort()+"\n";
		result += "X Coordinate: "+getXCoordinate()+"\n";
		result += "Y Coordinate: "+getYCoordinate()+"\n";
		result += "Links: ";
		for(int i = 0; i < links.size(); i++)
		{
			result += links.get(i).toString()+" "; 
		}
		result += "\n";
		return result;
	}
	
	public void moveInArea()
	{
	  area.moveNode(this);
	}
	
	@Override
	public boolean equals(Object o)
	{
	  if(o instanceof NetworkNode)
	  {
	    if(((NetworkNode)o).getNumber() == this.getNumber())
	    {
	      return true;
	    }
	  }
	  return false;
	}
	
	@Override
	public int hashCode()
	{
	  return this.getNumber();
	}

}
