package com.wireless.main;


public class PacketDropRateCalculator
{
  int[][] lastPacketFromLink;
  int[][] totalPacktsLostFromLink;
  
  /*
   * Initializes the class object with a network configuration.
   */
  public PacketDropRateCalculator()
  {
    int numberOfNodes = ObjectSingleton.getInstance().getNetworkConfiguration().getNodes().size();
    //int numberOfNodes = 2;
    lastPacketFromLink = new int[numberOfNodes][numberOfNodes];
    totalPacktsLostFromLink = new int[numberOfNodes][numberOfNodes];
    for(int i = 0; i < numberOfNodes; i++)
    {
      for(int j = 0; i < numberOfNodes; i++)
      {
	lastPacketFromLink[i][j] = -1;
      }
    }
  }

  //Returns the number of packets dropped between the new packet and the previos packet
  // packet - the new packet received.
  public int addPacketAndCalculatePacketDrop(Packet packet)
  {
    int lastValue = lastPacketFromLink[packet.getSourceAddress()-1][packet.getLastLocation()-1];
    int lostPackets;
    if(lastValue > packet.getMaxSequenceNumber() - 100)
    {
      lostPackets = packet.getSequenceNumber() - (packet.getMaxSequenceNumber() - lastValue) - 1;

    }
    else
    {
      //System.out.println("lastvalue: "+lastValue);
      lostPackets = packet.getSequenceNumber() - lastValue - 1;
    }
    lastPacketFromLink[packet.getSourceAddress()-1][packet.getLastLocation()-1] = packet.getSequenceNumber();
    return lostPackets;
  }
  
 //Returns the total packets lost for the node that run 
 //packet - the new packet received
 public int getAndUpdateTotalPacketsLost(Packet packet)
 {
   totalPacktsLostFromLink[packet.getSourceAddress()-1][packet.getLastLocation()-1] += addPacketAndCalculatePacketDrop(packet);
   return totalPacktsLostFromLink[packet.getSourceAddress()-1][packet.getLastLocation()-1];
 }
  
  
}
