package com.wireless.main;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.wireless.mobility.MobileArea;
import com.wireless.mobility.MobileNodesGui;

/*
 * This is the main class of the program.
 * It creates the first two GUI pages and maintains how they interact.
 */
public class wirelessproject3 {

	//The number of nodes in the ad hoc network
	static int totalNodeCount = 16;
	
	// The array that contains the windows for the nodes on the computer
	final static ArrayList<TransmissionWindow> twArray = new ArrayList<TransmissionWindow>();

	final static int SECONDS_TO_WAIT = 1;
	/*
	 * The main method of the program.  Starts by building the GUI.
	 */
	public static void main(String[] args) 
	{
		// The array that c
		//buildGUI();
	   takeInputs();


	}
	
	public static void takeInputs()
	{
	  Scanner userInput = new Scanner(System.in);
	  System.out.println("Please input a configuration file.");
	  String configFileName = userInput.next();
	  System.out.println(configFileName);
	  //System.out.println("Please input the computer name.");
	  //String computerName = userInput.next();
	  String computerName = "";
	  try
	  {
	    computerName = InetAddress.getLocalHost().getHostName();
	  } catch (UnknownHostException e2)
	  {
	    // TODO Auto-generated catch block
	    e2.printStackTrace();
	    System.exit(1);
	  }
	  NetworkConfiguration nc = new NetworkConfiguration();
	  ObjectSingleton oSing = ObjectSingleton.getInstance();
	  //oSing.setNetworkConfiguration(nc);
	  try 
	  {
		nc.readConfigurationFile(configFileName);
	  } 
	  catch (IOException e1) 
	  {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	  }
	  oSing.setComputerName(computerName);
	  ArrayList<NetworkNode> nodes = nc.getComputerNodeList();
	  for(int i = 0; i < nodes.size(); i++)
	  {
	    try
	    {
	      //need to finish this later and add the arguments.
	      System.out.println("Executing: %programfiles%\\Java\\jre7\\bin\\javaw.exe -agentlib:jdwp=transport=dt_socket,suspend=y,address=localhost:55646 -Dfile.encoding=Cp1252 -classpath \"C:\\Users\\Zack\\Zack'sPrograms\\eclipse_workspace\\Wireless_Networks_Project3\\src\" com.wireless.main.TransmissionWindow "+configFileName+" "+computerName+" "+nodes.get(i).getNumber());
	      Runtime.getRuntime().exec( "%programfiles%\\Java\\jre7\\bin\\javaw.exe -agentlib:jdwp=transport=dt_socket,suspend=y,address=localhost:55646 -Dfile.encoding=Cp1252 -classpath \"C:\\Users\\Zack\\Zack'sPrograms\\eclipse_workspace\\Wireless_Networks_Project3\\src\" com.wireless.main.TransmissionWindow " 
	      		//"\"C:\Users\Zack\Zack'sPrograms\eclipse_workspace\Wireless_Networks_Project3\src\com\wireless\main\fixed.txt\" HP_envy 2")
	      		+ configFileName+" "+computerName+" "+nodes.get(i).getNumber());
	      //Runtime.getRuntime().exec("java TransmissionWindow.java "+configFileName+
		//  " "+computerName+" "+nodes.get(i).getNumber());
	      
	    } catch (IOException e)
	    {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    } 
	    
	  }
	  userInput.close();
	  
	  
	  
	}
	

	/*
	 * Creates the first two GUI's and defines their interactions
	 */
	/*
	public static void buildGUI()
	{
		//first GUI
		final JFrame frame1 = new JFrame("Flooding Algorithm Communicator - Configuration File");
		//second GUI
		final JFrame frame2 = new JFrame("Flooding Algorithm Communicator - Computer Name Input");
		
		//create first GUI
		JPanel mainPanel = new JPanel();
		
		mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.PAGE_AXIS));
		//frame2Panel.setLayout(new BoxLayout(frame2Panel,BoxLayout.PAGE_AXIS));
		JPanel panel1 = new JPanel();
		Container contentPane = frame1.getContentPane();
		
		//contentPane.setLayout(new SpringLayout());
		JLabel configurationFileLabel = new JLabel("Configuration File:",JLabel.TRAILING);;
		final JTextField configurationFileText = new JTextField(30);
		mainPanel.add(panel1);
		final JButton okButton = new JButton("Ok");
		final ConfigurationFileWatcher cfw = new ConfigurationFileWatcher();
		final Thread thread1 = new Thread(cfw);
		ObjectSingleton.getInstance().setIsConfigurationRunning(false);
		okButton.addMouseListener(new MouseAdapter()
		{
			//handle mouse click for the ok button
			//reads the file name and starts the next GUI
			@Override
            public void mouseClicked(MouseEvent e) 
			{
				String fileName = configurationFileText.getText();
				NetworkConfiguration nc = new NetworkConfiguration();
				ObjectSingleton oSing = ObjectSingleton.getInstance();
				oSing.setNetworkConfiguration(nc);
				oSing.setConfigurationFilename(fileName);
				try {
					nc.readConfigurationFile(fileName);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				frame1.setVisible(false);
				frame2.setVisible(true);
				cfw.setIsRunning(true);
				if(!ObjectSingleton.getInstance().getIsConfigurationRunning())
				{
				  thread1.start();
				  ObjectSingleton.getInstance().setIsConfigurationRunning(true);
				}
				
			}
			
			
		});
		mainPanel.add(okButton);
		panel1.add(configurationFileLabel);
		configurationFileLabel.setLabelFor(configurationFileText);
		panel1.add(configurationFileText);
		contentPane.add(mainPanel);
		frame1.pack();
		frame1.setSize(600,200);
		frame1.setVisible(true);
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		
		//builds the second GUI
        JPanel mainPanel2 = new JPanel();
		
		mainPanel2.setLayout(new BoxLayout(mainPanel2,BoxLayout.PAGE_AXIS));
		//frame2Panel.setLayout(new BoxLayout(frame2Panel,BoxLayout.PAGE_AXIS));
		JPanel panel2 = new JPanel();
		Container contentPane2 = frame2.getContentPane();
		
		//contentPane.setLayout(new SpringLayout());
		JLabel computerNameLabel = new JLabel("Computer Name:",JLabel.TRAILING);;
		final JTextField computerNameText = new JTextField(30);
		mainPanel2.add(panel2);
		JLabel nodeNumberLabel = new JLabel("Node Number:",JLabel.TRAILING);;
		final JTextField nodeNumberText = new JTextField(30);
		mainPanel2.add(panel2);
		final JButton okButton2 = new JButton("Ok");
		okButton2.addMouseListener(new MouseAdapter()
		{
			//makes the GUI invisible when the OK button is pressed
			//also starts multiple transmission windows, one for 
			//each node on the computer.
			@Override
            public void mouseClicked(MouseEvent e) 
			{
				frame2.setVisible(false);
				ObjectSingleton.getInstance().setComputerName(computerNameText.getText());
				createNodeWindows();
				
			}
			
			public void createNodeWindows()
			{
				ArrayList<NetworkNode> computerNodes = ObjectSingleton.getInstance().getNetworkConfiguration().getComputerNodeList();
                twArray.clear();
                
                if(computerNodes.size() == 1)
                {
                	TransmissionWindow tw = new TransmissionWindow(cfw, computerNodes.get(0).getNumber(),totalNodeCount,
							computerNodes.get(0).getPort());
					  twArray.add(tw);
					  tw.makeWindow();
                }
                else
                {
                  Integer nodeNumber = new Integer(nodeNumberText.getText());
                  for(int i = 0; i < computerNodes.size(); i++)
                  {
                	NetworkNode node = computerNodes.get(i);
                	if(node.getNumber() == nodeNumber.intValue())
                	{
                	  //System.out.println("Match");
                	  //int index = computerNodes.indexOf(nodeNumber);
                	  //System.out.println("Index: "+index);
			  TransmissionWindow tw = new TransmissionWindow(cfw, node.getNumber(),totalNodeCount,
							node.getPort());
			  twArray.add(tw);
			  tw.makeWindow();
                	}
                  }
					
				}
			}
		});
		mainPanel2.add(okButton2);
		panel2.add(computerNameLabel);
		computerNameLabel.setLabelFor(computerNameText);
		panel2.add(computerNameText);
		//panel2.add(nodeNumberLabel);
		//nodeNumberLabel.setLabelFor(nodeNumberText);
		//panel2.add(nodeNumberText);
		contentPane2.add(mainPanel2);
		frame2.pack();
		frame2.setSize(800,200);
		frame2.setVisible(false);
		//int threadID = ThreadIDGenerator.getID();
		//ThreadIDStack.getInstance().push(threadID);
		//SoundTransmissionRunningManager.getInstance().addThread(threadID, true);
		//Thread playThread = new Thread(new PlayThread(threadID,
		//		new FloodingProtocolManager(0, 0, totalNodeCount))); //current node doesn't matter for the playing thread
	    //playThread.start();
	    frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	}
	*/
	
	/*
	 * Returns the array of the transmissions windows that have
	 * been created.
	 */
	public static ArrayList<TransmissionWindow> getTransmissionWindows()
	{
		return twArray;
	}
	

}
