package com.wireless.main;


public class PacketDropRateTest 
{
  public static void main(String args[])
  {
	  PacketDropRateAlgorthim pdra = new PacketDropRateAlgorthim();
	  pdra.setDistance(100);
	  System.out.println("Packet loss at 100: "+pdra.calculateErrorRate());
	  pdra.setDistance(30);
	  System.out.println("Packet loss at 30: "+pdra.calculateErrorRate());
	  pdra.setDistance(120);
	  System.out.println("Packet loss at 120: "+pdra.calculateErrorRate());
	  
  }
}
