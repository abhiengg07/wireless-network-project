package com.wireless.main;

import java.net.DatagramSocket;
import java.net.SocketException;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import com.wireless.tables.OLSRProtocolManager;

/*
 * The thread for recording and sending sound
 */
public class RecordAndSendThread implements Runnable
{

	byte[] buffer;
	
	// the flooding protocol manager for the thread
	OLSRProtocolManager opm;
	
	DatagramSocket sender = null;
	//set up microphone
	TargetDataLine line = null;
	
	int destinationAddress = 0;
	
    /*
     * The constructor for the thread
     * 
     * newThreadID - the id of the thread
     * newFPM - the flooding protocol manager of the thread
     */
	public RecordAndSendThread(OLSRProtocolManager newOPM)
	{
		opm = newOPM;
	}
	
	/*
	 * Records the information from the microphones and
	 * then sends to the flooding protocol manager which transmits
	 * the sound.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	
	public void setUpRecording() {
		//set up computer connection
		AudioFormatSingleton afs = AudioFormatSingleton.getInstance();
		//DatagramPacket dp = null;
		
		try {
			sender = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, afs.getFormat());
        try 
        {
			line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(afs.getFormat());
        } 
        catch (LineUnavailableException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
        line.start();
        
      
        opm.setSendingSocket(sender);
        
        //record microphone information
        
        int bufferSize = (int)afs.getFormat().getSampleRate() * afs.getFormat().getFrameSize();
        System.out.println("BufferSize: "+bufferSize);
        buffer = new byte[bufferSize];
	}
	
	public void record()
	{
	if(destinationAddress == 0)
	{
	  System.out.println("No destination to send message to.");
	}
	else
	{
          System.out.println("Recording");
        //SoundTransmissionRunningManager strm = SoundTransmissionRunningManager.getInstance();
        //while(strm.isThreadRunning(threadID))
        //{ 
        	int count = line.read(buffer, 0, buffer.length);
        	if(count > 0)
        	{
        	  
        		opm.sendNewMessage(buffer,destinationAddress);
        	}
        //}
        //SoundTransmissionRunningManager.getInstance().removeThread(threadID);
	}
    }
        
    public void setDestinationAddress(int destinationAddress)
    {
      this.destinationAddress = destinationAddress;
    }
	
        
    public void tearDownRecording()
    {
    	sender.close();
        sender.disconnect();
        line.close();
	}

	@Override
	public void run() {
		setUpRecording();
		while(ObjectSingleton.getInstance().getIsRecording())
		{
			record();
		}
		tearDownRecording();
		
	}

}
