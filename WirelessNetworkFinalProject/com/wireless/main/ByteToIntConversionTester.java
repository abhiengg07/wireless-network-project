package com.wireless.main;

/*
 * This class is a test class and not part of the 
 * main project
 */
public class ByteToIntConversionTester 
{
  public static void main(String args[])
  {
	  Packet p = new Packet();
	  int test = 300;
	  int testBytesSize = 4;
	  System.out.println("Original Value: "+test);
	  byte[] b = PacketUtils.intToByteArray(test, testBytesSize);
	  System.out.println("The newly generated bytes: ");
	  for(int i = 0; i < testBytesSize; i++)
	  {
		  System.out.println("byte "+i+" : "+b[i]);
	  }
	  test = PacketUtils.byteArrayToInt(b);
	  System.out.println("Final Value: "+test); 
	  
	  byte[] b2 = new byte[2];
	  b2[0] = 4;
	  b2[1] = 0;
	  System.out.println("b2: "+PacketUtils.byteArrayToInt(b2));
			  
  }
}
