package com.wireless.main;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import com.wireless.tables.HelloMessageController;
import com.wireless.tables.NeighborTable;
import com.wireless.tables.OLSRProtocolManager;
import com.wireless.tables.TopologyControlMessage;
import com.wireless.tables.TopologyControlMessageController;

/*
 * The class that handles the transmission windows.
 */
public class TransmissionWindow implements ActionListener
{

  // the file change watcher for the configuration file
  ConfigurationFileWatcher cfw;
  // the reference to the file input frame
  JFrame fileFrame;
  // the reference to inputting the computer name frame
  JFrame computerNameFrame;
  // the frame for the transmission window.
  final JFrame transmissionWindowFrame = new JFrame();
  // the node number of the frame
  int nodeNumber;
  // the idStack for threads
  ThreadIDStack idStack = ThreadIDStack.getInstance();
  // the olsr protocol manager
  OLSRProtocolManager opm;
  // the port number for the transmission window
  int port;

  
  public static void main(String args[])
  {
   System.out.println("Running");
   if(args.length != 3)
   {
     System.err.println("Invalid number of arguements");
   }
   else
   {
      String configFilename = args[0];
      String computerName = args[1];
      String nodeNumber = args[2];
      // cfw = new ConfigurationFileWatcher();
      NetworkConfiguration nc = new NetworkConfiguration();
      try
      {
	//System.out.println("ConfigFile: "+configFilename);
	nc.readConfigurationFile(configFilename);
      } catch (IOException e1)
      {
	// TODO Auto-generated catch block
	e1.printStackTrace();
      }
      ObjectSingleton.getInstance().setNetworkConfiguration(nc);
      ObjectSingleton.getInstance().setComputerName(computerName);
      ObjectSingleton.getInstance().setConfigurationFilename(configFilename);
      TransmissionWindow tw = new TransmissionWindow(new Integer(nodeNumber).intValue());
      tw.makeWindow(); 
   }
  }
  
  
  /*
   * the constructor for the TransmissionWindow class; it starts the receiving
   * thread
   * 
   * newCFw - the configuration file watcher newFileFrame - the file watching
   * frame newComputerNameFrame - the computer name frame newNodeNumber - the
   * new number of the node totalNodeCount - the count of the nodes in the
   * network newPort - the port for the transmission to recieve on
   */
  public TransmissionWindow(
       int newNodeNumber)
  {
    cfw =  new ConfigurationFileWatcher();
    /*if(ObjectSingleton.getInstance().getNetworkConfiguration() == null)
    {
      System.out.println("1");
    }
    if()*/
    nodeNumber = newNodeNumber;
    //System.out.println(nodeNumber);
    //System.out.println(ObjectSingleton.getInstance().getNetworkConfiguration() == null);
    //System.out.println(ObjectSingleton.getInstance().getNetworkConfiguration().getNodeBasedOnNumber(nodeNumber) == null);
    port = ObjectSingleton.getInstance().getNetworkConfiguration().getNodeBasedOnNumber(nodeNumber).getPort();
    NetworkConfiguration config = ObjectSingleton.getInstance().getNetworkConfiguration();
    NeighborTable nt = new NeighborTable();
    HelloMessageController hmc = new HelloMessageController(nt,config.getNodeBasedOnNumber(nodeNumber));
    TopologyControlMessageController tcmc = new TopologyControlMessageController(config.getNodeBasedOnNumber(nodeNumber),
	nt, config);
    opm = new OLSRProtocolManager(config,
	config.getNodeBasedOnNumber(nodeNumber), hmc, tcmc);
    int threadID = ThreadIDGenerator.getID();
    ThreadIDStack.getInstance().push(threadID);
    SoundTransmissionRunningManager.getInstance().addThread(threadID, true);
    Thread receivingThread = new Thread(new ReceiveAndSendThread(threadID, opm,
	port));
    cfw.setIsRunning(true);
    Thread configurationThread = new Thread(cfw);
    receivingThread.start();
    configurationThread.start();
  }

  /*
   * creates the transmission window GUI
   */
  public void makeWindow()
  {
    transmissionWindowFrame
	.setTitle("Flooding Algorithm Communicator - Recorder (Node "
	    + new Integer(nodeNumber).toString() + ")");
    JPanel frame2Panel = new JPanel();
    final JTextField destinationText = new JTextField(30);
    Container contentPane2 = transmissionWindowFrame.getContentPane();
    final JButton transmitButton = new JButton("Transmit");
    transmitButton.addMouseListener(new MouseAdapter()
    {
      // performs the actions required to stop and start the transmission
      @Override
      public void mouseClicked(MouseEvent e)
      {
	// SoundTransmissionRunningManager strm =
	// SoundTransmissionRunningManager.getInstance();
	if (transmitButton.getText().equals("Transmit"))
	{
	  transmitButton.setText("Stop");
	  RecordAndSendThread rastThread = new RecordAndSendThread(opm);
	  rastThread.setDestinationAddress(new Integer(destinationText.getText()).intValue());
	  ObjectSingleton.getInstance().setIsRecording(true);
	  Thread rast = new Thread(rastThread);
	  rast.start();

	} else
	{
	  ObjectSingleton.getInstance().setIsRecording(false);
	  transmitButton.setText("Transmit");
	}
      }

    });
    final JButton newConfigurationFileButton = new JButton(
	"New Configuration File");
    newConfigurationFileButton.addMouseListener(new MouseAdapter()
    {
      // handles the actions for new file button
      @Override
      public void mouseClicked(MouseEvent e)
      {
	transmissionWindowFrame.setVisible(false);
	fileFrame.setVisible(true);
	cfw.setIsRunning(false);
	ArrayList<TransmissionWindow> twArray = wirelessproject3
	    .getTransmissionWindows();
	for (int i = 0; i < twArray.size(); i++)
	{
	  twArray.get(i).setInvisible();
	}
      }
    });

    final JButton newComputerNameButton = new JButton("New Computer Name");
    newComputerNameButton.addMouseListener(new MouseAdapter()
    {
      // handles the action for the new computer name button
      @Override
      public void mouseClicked(MouseEvent e)
      {
	transmissionWindowFrame.setVisible(false);
	computerNameFrame.setVisible(true);
	ArrayList<TransmissionWindow> twArray = wirelessproject3
	    .getTransmissionWindows();
	for (int i = 0; i < twArray.size(); i++)
	{
	  twArray.get(i).setInvisible();
	}
      }
    });

    JRadioButton muteButton = new JRadioButton("Mute");
    muteButton.setMnemonic(KeyEvent.VK_M);
    muteButton.setActionCommand("Mute");
    muteButton.setSelected(true);

    JRadioButton playButton = new JRadioButton("Play");
    playButton.setMnemonic(KeyEvent.VK_P);
    playButton.setActionCommand("Play");

    // Group the radio buttons.
    ButtonGroup group = new ButtonGroup();
    group.add(muteButton);
    group.add(playButton);

    JPanel radioPanel = new JPanel(new GridLayout(0, 1));
    radioPanel.add(muteButton);
    radioPanel.add(playButton);

    frame2Panel.add(transmitButton);
    frame2Panel.add(newConfigurationFileButton);
    frame2Panel.add(newComputerNameButton);
    frame2Panel.add(radioPanel);

    JPanel outsidePanel = new JPanel();
    outsidePanel.setLayout(new BoxLayout(outsidePanel, BoxLayout.Y_AXIS));
    outsidePanel.add(frame2Panel);
    JPanel destinationPanel = new JPanel();
    JLabel destinationLabel = new JLabel("Destination Address: ");
    //destinationLabel.setSize(60, 20);
    destinationPanel.add(destinationLabel);
    //destinationText.setSize(60, 20);
    destinationPanel.add(destinationText);
    outsidePanel.add(destinationPanel);
    
    muteButton.addActionListener(this);
    playButton.addActionListener(this);

    contentPane2.add(outsidePanel);
    transmissionWindowFrame.pack();
    transmissionWindowFrame.setSize(600, 200);
    transmissionWindowFrame.setVisible(true);
    transmissionWindowFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public void setInvisible()
  {
    transmissionWindowFrame.setVisible(false);
    SoundTransmissionRunningManager.getInstance().removeThread(nodeNumber); // need
  }

  public void actionPerformed(ActionEvent e)
  {
    if (e.getActionCommand().equals("Mute"))
    {
      ObjectSingleton.getInstance().setIsPlaying(false);
      System.out.println("Muted");
    } else
    {
      ObjectSingleton.getInstance().setIsPlaying(true);
      System.out.println("Playing");
    }
  }
}
