package com.wireless.tables;

import com.wireless.main.NetworkNode;

public class RoutingTableItem
{

  NetworkNode destination;
  NetworkNode nextHop;
  int hops;
  
  public RoutingTableItem()
  {
    
  }
  
  public RoutingTableItem(NetworkNode destination, NetworkNode nextHop,
      int hops)
  {
    this.destination = destination;
    this.nextHop = nextHop;
    this.hops = hops;
  }

  public NetworkNode getDestination()
  {
    return destination;
  }

  public void setDestination(NetworkNode destination)
  {
    this.destination = destination;
  }

  public NetworkNode getNextHop()
  {
    return nextHop;
  }

  public void setNextHop(NetworkNode nextHop)
  {
    this.nextHop = nextHop;
  }

  public int getDistance()
  {
    return hops;
  }

  public void setDistance(int hops)
  {
    this.hops = hops;
  }

  
}
