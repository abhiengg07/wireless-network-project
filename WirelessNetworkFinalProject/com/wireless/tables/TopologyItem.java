package com.wireless.tables;

import com.wireless.main.NetworkNode;

public class TopologyItem
{

  NetworkNode destination;
  NetworkNode lastHop;
  int sequenceNumber;
  long time;
  
  public TopologyItem()
  {
    time = System.currentTimeMillis();
  }
  
  public long getTime()
  {
    return time;
  }

  public void setTime(long time)
  {
    this.time = time;
  }

  public TopologyItem(NetworkNode destination, NetworkNode lastHop,
      int sequenceNumber)
  {
    this.destination = destination;
    this.lastHop = lastHop;
    this.sequenceNumber = sequenceNumber;
  }

  public NetworkNode getDestination()
  {
    return destination;
  }

  public void setDestination(NetworkNode destination)
  {
    this.destination = destination;
  }

  public NetworkNode getLastHop()
  {
    return lastHop;
  }

  public void setLastHop(NetworkNode lastHop)
  {
    this.lastHop = lastHop;
  }

  public int getSequenceNumber()
  {
    return sequenceNumber;
  }

  public void setSequenceNumber(int sequenceNumber)
  {
    this.sequenceNumber = sequenceNumber;
  }
  
  public boolean isDestination(NetworkNode possibleDestination)
  {
    return destination.equals(possibleDestination);
  }

}
