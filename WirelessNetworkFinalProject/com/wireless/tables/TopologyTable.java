package com.wireless.tables;

import java.util.ArrayList;

import com.wireless.main.NetworkNode;

public class TopologyTable
{

  ArrayList<TopologyItem> topologyArray = new ArrayList<TopologyItem>();
  final int TIME_TO_REMOVE = 5;
  
  
  public TopologyTable()
  {
    
  }
  
  public boolean updateTable(NetworkNode destination, NetworkNode lastHop,
      int sequenceNumber)
  {
     boolean tableContainsDestination = false;
     TopologyItem item = null;
     boolean result = true;
     for(int i = 0; i < topologyArray.size(); i++)
     {
       if(topologyArray.get(i).isDestination(destination));
       {
	 tableContainsDestination = true;
	 item = topologyArray.get(i);
	 break;
       }
     }
     if(tableContainsDestination)
     {
       if(item.getSequenceNumber() < sequenceNumber)
       {
         if(item.getLastHop().equals(lastHop)) //after this if would be the place to 
	                                     //increase holding time if you put it in
         {
	   item.setSequenceNumber(sequenceNumber);
	   item.setTime(System.currentTimeMillis());
         }
         else 
         {
	   item.setSequenceNumber(sequenceNumber);
	   item.setLastHop(lastHop);
	   item.setTime(System.currentTimeMillis());
         }
       }
       {
	 result = false;
       }
     }
     else
     {
       topologyArray.add(new TopologyItem(destination, lastHop, sequenceNumber));
     }
     return result;
  }
  
  public void clearOldEntries()
  {
    for(int i = topologyArray.size(); i > -1; i--)
    {
      if(topologyArray.get(i).getTime() + TIME_TO_REMOVE * 1000 < System.currentTimeMillis())
      {
	topologyArray.remove(i);
      }
      
    }
  }
  
  public ArrayList<TopologyItem> getTopologyArray()
  {
    return topologyArray;
  }

}
