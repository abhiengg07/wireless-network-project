package com.wireless.tables;

import java.util.Random;

import com.wireless.main.NetworkNode;

public class HelloMessageController
{
  final long TIME_BETWEEN_MESSAGES = 5L;
  long timeUntilFirstMessage;
  long nextTimeToSend;
  Random rand = new Random();
  NeighborTable neighborTable;
  NetworkNode currentNode;
  
  public HelloMessageController(NeighborTable neighborTable, NetworkNode currentNode)
  {
    timeUntilFirstMessage = (long)(rand.nextInt(2000)+1000);
    nextTimeToSend = System.currentTimeMillis() + timeUntilFirstMessage;
    this.neighborTable = neighborTable;
    this.currentNode = currentNode;
  }
  
  public HelloMessage sendMessage()
  {
    if(nextTimeToSend < System.currentTimeMillis())
    {
      nextTimeToSend = System.currentTimeMillis() + TIME_BETWEEN_MESSAGES;
      return new HelloMessage(neighborTable, currentNode);
    }
    else
    {
      return null;
    }
  }

}
