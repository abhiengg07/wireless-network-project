package com.wireless.tables;

import java.util.ArrayList;

import com.wireless.main.NetworkConfiguration;
import com.wireless.main.NetworkNode;

public class TopologyControlMessageController
{
  NetworkNode currentNode;
  NeighborTable neighborTable;
  long lastSentTime;
  final int TIME_BETWEEN_MESSAGES = 5;
  final int TIME_BEFORE_FIRST_MESSAGE = 7;
  ArrayList<MPRSequencePair> mPRSelector = new ArrayList<MPRSequencePair>();
  NetworkConfiguration config;
  
  public TopologyControlMessageController(NetworkNode currentNode, NeighborTable neighborTable, NetworkConfiguration config)
  {
    this.currentNode = currentNode;
    this.neighborTable = neighborTable;
    this.config = config;
    lastSentTime = System.nanoTime() + TIME_BEFORE_FIRST_MESSAGE * 1000000000 ;
  }
  
  public TopologyControlMessage sendMessage()
  {
    if(lastSentTime + TIME_BETWEEN_MESSAGES * 100000000 > System.nanoTime())
    {
      return null;
    }
    else
    {
      lastSentTime = System.nanoTime();
      if(mPRSelector.size() > 0 && !areEqual(mPRSelector, neighborTable.getMPRSelectors()))
      {
	mPRSelector = neighborTable.getMPRSelectors();
	return new TopologyControlMessage(currentNode, mPRSelector,null);
      }
      return null;
    }
  }
  
  private boolean areEqual(ArrayList<MPRSequencePair> list1, ArrayList<MPRSequencePair> list2)
  {
    if(list1.size() != list2.size())
    {
      return false;
    }
    else
    {
      for(int i = 0; i < list1.size(); i++)
      {
	if(!list1.get(i).getNode().equals(list2.get(i).getNode()))
	{
	  return false;
	}
      }
      return true;
    }
  }
}
