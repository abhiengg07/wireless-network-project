package com.wireless.tables;

import java.util.ArrayList;

import com.wireless.main.NetworkConfiguration;
import com.wireless.main.NetworkNode;
import com.wireless.main.PacketUtils;

public class TopologyControlMessage
{
  ArrayList<MPRSequencePair> mPRSelectors;
  byte[] packet;
  final int SIZE_OF_HEADER = 2;
  final int SIZE_OF_SEQUENCE_NUMBER = 4;
  final int SIZE_OF_NODE = 2;
  int messagePointer = 0;
  NetworkConfiguration config;
  NetworkNode messageNode;
  
  
  public TopologyControlMessage(NetworkNode messageNode, ArrayList<MPRSequencePair> mPRSelectors,
      NetworkConfiguration config)
  {
    this.messageNode = messageNode;
    this.mPRSelectors = mPRSelectors;
    this.config = config;
    packet = new byte[SIZE_OF_HEADER + mPRSelectors.size() * (SIZE_OF_SEQUENCE_NUMBER + SIZE_OF_NODE)];
    
  }
  


  public NetworkNode getMessageNode()
  {
    return messageNode;
  }

  public void setMessageNode(NetworkNode messageNode)
  {
    this.messageNode = messageNode;
  }

  public byte[] makeMessage()
  {
    messagePointer = PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(0, 1), messagePointer, packet);
    messagePointer = PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(2, 1), messagePointer, packet);
    messagePointer = PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(messageNode.getNumber(), SIZE_OF_NODE), messagePointer, packet);
    for(int i = 0; i < mPRSelectors.size(); i++)
    {
      addMPRtoMessage(mPRSelectors.get(i));
    }
    //create header
    //in header have message type identifier
    //Then have number of mprs
    //then list the mpr and sequence number pairs
    //see how many pairs you can hold after the header file
    //make sure you don't go over that or send another packet.
    return packet;
  }
  
  private void addMPRtoMessage(MPRSequencePair pair)
  {
    PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(pair.getNode().getNumber(), SIZE_OF_NODE), messagePointer, packet);
    PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(pair.getSequenceNumber(), SIZE_OF_SEQUENCE_NUMBER), messagePointer, packet);
  }
  
  //will need to include code to create the message object from a byte 
  //array and extract the information out.
  
  public static boolean isTopologyControlMessage(byte[] packet)
  {
    if(packet[0] == 0 && packet[1] == 2)
    {
      return true;
    }
    return false;
  }
  
  public void extractTopologyInformation(byte[] newPacket)
  {
    packet = newPacket;
    messagePointer = SIZE_OF_HEADER;
    mPRSelectors = new ArrayList<MPRSequencePair>();
    MPRSequencePair pair;
    
    while(messagePointer < packet.length)
    {
    
      pair = new MPRSequencePair(extractNodeFromMessage(messagePointer), 
	  extractSequenceNumber(messagePointer + SIZE_OF_NODE));
      mPRSelectors.add(pair);
      messagePointer += SIZE_OF_NODE + SIZE_OF_SEQUENCE_NUMBER;
    }
  }
  
  public ArrayList<MPRSequencePair> getMPRPairs()
  {
    return mPRSelectors;
  }
  
  private NetworkNode extractNodeFromMessage(int pos)
  {
    byte[] sequenceNumberArray = new byte[SIZE_OF_NODE];
    int j = 0;
    for(int i = pos; i < SIZE_OF_NODE; i++)
    {
      sequenceNumberArray[j] = packet[i];
      j++;
    }
    return config.getNodeBasedOnNumber(PacketUtils.byteArrayToInt(sequenceNumberArray));
    
  }
  
  private int extractSequenceNumber(int pos)
  {
    byte[] sequenceNumberArray = new byte[SIZE_OF_SEQUENCE_NUMBER];
    int j = 0;
    for(int i = pos; i < SIZE_OF_SEQUENCE_NUMBER; i++)
    {
      sequenceNumberArray[j] = packet[i];
      j++;
    }
    return PacketUtils.byteArrayToInt(sequenceNumberArray);
  }
  
}
