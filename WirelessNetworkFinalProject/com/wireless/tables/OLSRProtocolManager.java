package com.wireless.tables;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

import com.wireless.main.LengthException;
import com.wireless.main.NetworkConfiguration;
import com.wireless.main.NetworkNode;
import com.wireless.main.ObjectSingleton;
import com.wireless.main.Packet;
import com.wireless.main.PacketDropRateAlgorthim;
import com.wireless.tables.NeighborTableItem.ConnectionType;

public class OLSRProtocolManager
{
  NetworkConfiguration config;
  HelloMessageController helloMessageController;
  TopologyControlMessageController topologyControlMessageController;
  NeighborTable neighborTable = new NeighborTable();
  TopologyTable topologyTable = new TopologyTable();
  RoutingTable routingTable;
  NetworkNode currentNode;
  final int RANGE = 110;
  DatagramSocket sender = null;
  int[] cacheTable;
  int currentSequenceNumber = 0;
  int soundSequenceNumber = 0;

  public OLSRProtocolManager(NetworkConfiguration config,
      NetworkNode currentNode, HelloMessageController helloMessageController,
      TopologyControlMessageController topologyControlMessageController)
  {
    this.config = config;
    this.currentNode = currentNode;
    this.helloMessageController = helloMessageController;
    this.topologyControlMessageController = topologyControlMessageController;
    this.routingTable = new RoutingTable(this.currentNode, this.neighborTable,
	this.topologyTable);
    this.cacheTable = new int[this.config.getNodes().size()];
    for (int i = 0; i < this.config.getNodes().size(); i++)
    {
      cacheTable[i] = -1;
    }
  }

  public void update(byte[] packet)
  {
    if (packet != null)
    {

      if (HelloMessage.isHelloMessage(packet))
      {
	updateFromHelloMessage(packet);
      } else if (TopologyControlMessage.isTopologyControlMessage(packet))
      {
	updateFromTopologyMessage(packet);
      }
    }
    HelloMessage sendingHelloMessage = helloMessageController.sendMessage();
    if (sendingHelloMessage != null)
    {
      transmitHelloMessage(sendingHelloMessage);
    }
    TopologyControlMessage topologyControlMessage = topologyControlMessageController
	.sendMessage();
    if (topologyControlMessage != null)
    {
      transmitTopologyControlMessage(topologyControlMessage);
    }

  }

  private void transmitTopologyControlMessage(
      TopologyControlMessage topologyControlMessage)
  {
    ArrayList<NetworkNode> nodes = config.getNodes();
    for (int i = 0; i < nodes.size(); i++)
    {
      if (isInRange(nodes.get(i)))
      {
	transmitPacket(topologyControlMessage.makeMessage(), nodes.get(i)
	    .getIPAddress(), nodes.get(i).getPort());
      }
    }

  }

  private void transmitHelloMessage(HelloMessage sendingHelloMessage)
  {
    ArrayList<NetworkNode> nodes = config.getNodes();
    for (int i = 0; i < nodes.size(); i++)
    {
      if (isInRange(nodes.get(i)))
      {
	transmitPacket(sendingHelloMessage.makePacket(), nodes.get(i)
	    .getIPAddress(), nodes.get(i).getPort());
      }
    }

  }

  private void updateFromTopologyMessage(byte[] packet)
  {
    TopologyControlMessage tcm = new TopologyControlMessage(currentNode, null,
	config);
    tcm.extractTopologyInformation(packet);
    ArrayList<MPRSequencePair> mprPairs = tcm.getMPRPairs();
    boolean changed = false;
    for (int i = 0; i < mprPairs.size(); i++)
    {
      topologyTable.updateTable(mprPairs.get(i).getNode(),
	  tcm.getMessageNode(), mprPairs.get(i).getSequenceNumber());

    }
    neighborTable.clearOldEntries();
    topologyTable.clearOldEntries();
    routingTable.update();

  }

  private void updateFromHelloMessage(byte[] packet)
  {
    HelloMessage message = new HelloMessage(neighborTable,null);
    message.extractInformation(packet);

    // remove if in two hop array section
    ArrayList<TwoHopStructure> twoHopNodeStructures = neighborTable
	.get2HopNodes();
    TwoHopStructure thStructure;
    for (int i = twoHopNodeStructures.size(); i > -1; i--)
    {
      thStructure = twoHopNodeStructures.get(i);
      if (thStructure.getSecondHopNode()
	  .equals(message.getNodeSendingMessage()))
      {
	LinkedList<NeighborTableItem> list = neighborTable
	    .getItemsWithNode(thStructure.getSecondHopNode());
	for (NeighborTableItem item : list)
	{
	  neighborTable.removeItem(item);
	}
      }
    }
    // add to one hop table
    LinkedList<NetworkNode> oneHopNodeMessageList = new LinkedList<>();
    oneHopNodeMessageList.addAll(message.getUniDirectionalNodes());
    oneHopNodeMessageList.addAll(message.getBiDirectionalNodes());
    oneHopNodeMessageList.addAll(message.getMPRnodeList());

    // check if node is already in the current node's single hop list
    if (neighborTable.get1HopNodes().contains(message.nodeSendingMessage))
    {
      ArrayList<NetworkNode> oneHopNodes = neighborTable.get1HopNodes();
      NetworkNode node;
      Iterator<NetworkNode> listNodeIter = oneHopNodeMessageList.iterator();
      while (listNodeIter.hasNext())
      {
	node = listNodeIter.next();
	for (int i = oneHopNodes.size(); i > -1; i--)
	{
	  if (oneHopNodes.get(i).equals(node))
	  {
	    listNodeIter.remove();
	    oneHopNodes.remove(i);
	    break;
	  }
	}
      }
      for (int i = 0; i < oneHopNodes.size(); i++)
      {
	LinkedList<NeighborTableItem> items = neighborTable
	    .getItemsWithNode(oneHopNodes.get(i));
	for (NeighborTableItem item : items)
	{
	  item.setReferences(item.getReferences() - 1);
	  if (item.getReferences() < 1)
	  {
	    neighborTable.removeItem(item);
	  }
	}
      }
      for (NetworkNode node2 : oneHopNodeMessageList)
      {
	neighborTable.addItemToTable(node2, ConnectionType.TWO_HOP, 2, null,
	    null);
      }
    } else
    {

      // see if nodes are already in the table
      // if not, add them as a two hop entry
      // else update their reference
      for (NetworkNode node : oneHopNodeMessageList)
      {
	if (neighborTable.get1HopNodes().contains(node)
	    || neighborTable.get2HopNodesOnly().contains(node))
	{
	  LinkedList<NeighborTableItem> items = neighborTable
	      .getItemsWithNode(node);
	  for (NeighborTableItem item : items)
	  {
	    item.setReferences(item.getReferences() + 1);
	  }
	} else
	{
	  neighborTable.addItemToTable(node, ConnectionType.TWO_HOP, 2,
	      message.getNodeSendingMessage(), null);
	}
      }
    }
    // if in the package, try to see if node is already in table and change to
    // bidirectional
    if (oneHopNodeMessageList.contains(currentNode))
    {
      NeighborTableItem item = neighborTable.getNeighborTableItem(
	  message.getNodeSendingMessage(), ConnectionType.UNI_DIRECTIONAL);
      if (item != null)
      {
	item.setConnectionType(ConnectionType.BI_DIRECTIONAL);
      } else
      {
	item = neighborTable.getNeighborTableItem(
	    message.getNodeSendingMessage(), ConnectionType.BI_DIRECTIONAL);
	if (item == null)
	{
	  item = neighborTable
	      .getNeighborTableItem(message.getNodeSendingMessage(),
		  ConnectionType.MPR_FOR_THIS_NODE);
	  if (item == null)
	  {
	    neighborTable.addItemToTable(message.getNodeSendingMessage(),
		ConnectionType.BI_DIRECTIONAL, 1, null, oneHopNodeMessageList);
	  }
	}
      }
    }
    //if sending node has selected this node as a mpr selector put that
    //in the neighbor table
    for(int i = 0; i < message.getMprsOfTheNode().size(); i++)
    {
      if(message.getMprsOfTheNode().get(i).getNode().equals(currentNode))
      {
	neighborTable.addItemToTable(currentNode, ConnectionType.SELECTED_THIS_NODE_AS_MPR, 
	    1, null, null);
	break;
      }
    }
    neighborTable.clearOldEntries();
    neighborTable.selectMPRs();
    topologyTable.clearOldEntries();
    routingTable.update();

  }

  public void addReferenceForLink(LinkedList<NetworkNode> links)
  {
    for (NetworkNode node : links)
    {
      LinkedList<NeighborTableItem> linkItems = neighborTable
	  .getItemsWithNode(node);
      for (NeighborTableItem item : linkItems)
      {
	item.setReferences(item.getReferences() + 1);
      }
    }
  }

  /*
   * Sends packet to the specified IP Address and port using UDP.
   * 
   * packet - the packet to send ipAddress - the IP address of the destination
   * port - the port to send the packet to.
   */
  public void transmitPacket(byte[] packet, byte[] ipAddress, int port)
  {

    InetAddress ia = null;
    try
    {
      ia = InetAddress.getByAddress(ipAddress);
    } catch (UnknownHostException e2)
    {
      // TODO Auto-generated catch block
      e2.printStackTrace();
    }
    DatagramPacket dp = new DatagramPacket(packet, packet.length, ia, port);
    try
    {
      sender.send(dp);
    } catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  public boolean isInRange(NetworkNode node)
  {
    double distance = Math.sqrt(Math.pow(
	node.getXCoordinate() - currentNode.getXCoordinate(), 2)
	+ Math.pow(node.getYCoordinate() - currentNode.getYCoordinate(), 2));
    return distance < RANGE;
  }

  /*
   * Sets the sending datagram socket for the manager
   * 
   * newSocket - the new socket
   */
  public void setSendingSocket(DatagramSocket newSocket)
  {
    sender = newSocket;
  }

  /*
   * Determines if the packet originated from the current source. Returns true
   * if it did, and false if it did not. packet - the packet to check the source
   * of.
   */
  public boolean isFromSelf(Packet packet)
  {
    int source = packet.getSourceAddress();
    if (source == currentNode.getNumber())
    {
      return true;
    } else
    {
      return false;
    }
  }

  /*
   * Determines if the packet is new and if so, updates the cacheTable
   * accordingly. This should be called instead of isNewPacket when wanting to
   * update the table as well.
   * 
   * returns true if the packet is new and false otherwise
   * 
   * packet - the packet whose sequence number is checked
   */
  public boolean checkSeqeunceNumberOfPacket(Packet packet)
  {
    boolean isNew = isNewPacket(packet);
    if (isNew)
    {
      int packetSequenceNumber = packet.getSequenceNumber();
      int packetSource = packet.getSourceAddress();
      cacheTable[packetSource - 1] = packetSequenceNumber;

    }
    return isNew;

  }

  /*
   * Checks to see if the manager has received the packet or not.
   * 
   * returns true if the packet is new and false otherwise
   * 
   * packet - the packet to check if new.
   */
  public boolean isNewPacket(Packet packet)
  {
    int packetSequenceNumber = packet.getSequenceNumber();
    int packetSource = packet.getSourceAddress();
    boolean isNewPacket;
    if (!isFromSelf(packet))
    {
      if (cacheTable[packetSource - 1] > packet.getMaxSequenceNumber() - 100)
      {
	if (packetSequenceNumber < 100
	    || cacheTable[packetSource - 1] < packetSequenceNumber)
	{
	  isNewPacket = true;
	} else
	{
	  isNewPacket = false;
	}
      } else
      {
	if (cacheTable[packetSource - 1] < packetSequenceNumber)
	{
	  isNewPacket = true;
	} else
	{
	  isNewPacket = false;
	}
      }
    } else
    {
      isNewPacket = false;
    }
    return isNewPacket;
  }

  /*
   * Handles incrementing the sequence number. Used after creating a packet.
   */
  private void incrementSequenceNumber()
  {
    currentSequenceNumber++;
    if (currentSequenceNumber == (new Packet()).getMaxSequenceNumber())
    {
      currentSequenceNumber = 0;
    }
  }

  // ==================================================================================

  /*
   * The method that manages sending the packet to all of the necessary linked
   * nodes.
   * 
   * packet - the packet to send
   */
  public void forwardPacket(Packet packet)
  {
    RoutingTableItem route = routingTable.getRouteToDestination(config
	.getNodeBasedOnNumber(packet.getDestination()));

    // System.out.println("oldlinks: "+oldlinks+ " newlinks: "+newlinks+
    // " old last location: "+packet.getLastLocation()+" new last location: "+selfAddress);
    packet.setAndReplaceLastLocationInPacket(currentNode.getNumber());
    NetworkNode destination = route.getNextHop();
    System.out.println("Sending packet: " + packet.getSequenceNumber()
	+ " to node: " + destination.getNumber());
    transmitPacket(packet, destination.getIPAddress(), destination.getPort());

  }

  /*
   * The top method for sending a new message. It creates the packet for the
   * message and then sends out the packet to the nodes links.
   * 
   * message - the bytes to send as the information, does not include the
   * header.
   */
  public void sendNewMessage(byte[] message, int destinationAddress)
  {
    int packetInformationSize = (new Packet()).getInformationSize();
    int loopNumber = message.length / packetInformationSize;
    Packet packet;
    for (int i = 0; i < loopNumber + 1; i++)
    {
      packet = new Packet(currentSequenceNumber, currentNode.getNumber(),
	  destinationAddress, currentNode.getNumber());
      // System.out.println("Sending sequence number: "+currentSeqeunceNumber);
      incrementSequenceNumber();
      byte[] information;
      if (i == loopNumber)
      {
	information = Arrays.copyOfRange(message, i * packetInformationSize,
	    message.length);
      } else
      {
	information = Arrays.copyOfRange(message, i * packetInformationSize,
	    (i + 1) * packetInformationSize);
      }

      try
      {
	packet.createPacket(information);
      } catch (LengthException e)
      {
	// TODO Auto-generated catch block
	e.printStackTrace();
      }
      // System.out.println("Packet sequence number: "+packet.getSequenceNumber());
      forwardPacket(packet);

    }
  }

  /*
   * Sends packet to the specified IP Address and port using UDP.
   * 
   * packet - the packet to send ipAddress - the IP address of the destination
   * port - the port to send the packet to.
   */
  public void transmitPacket(Packet packet, byte[] ipAddress, int port)
  {

    InetAddress ia = null;
    try
    {
      ia = InetAddress.getByAddress(ipAddress);
    } catch (UnknownHostException e2)
    {
      // TODO Auto-generated catch block
      e2.printStackTrace();
    }
    /*
     * System.out.println("IP address: "); for(int i = 0; i < ipAddress.length;
     * i++) { System.out.println((short)ipAddress[i]+"."); }
     */
    // System.out.println("Sending packet: "+packet.getSequenceNumber()+" to port: "+port);

    DatagramPacket dp = new DatagramPacket(packet.getPacketBytes(),
	packet.getPacketSize(), ia, port);
    try
    {
      // System.out.println("Sending packet at the sender: "+packet.getSequenceNumber());
      sender.send(dp);
    } catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}