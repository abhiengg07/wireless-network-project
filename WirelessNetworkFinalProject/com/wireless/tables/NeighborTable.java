package com.wireless.tables;

import java.util.ArrayList;
import java.util.LinkedList;

import com.wireless.main.NetworkConfiguration;
import com.wireless.main.NetworkNode;
import com.wireless.main.ObjectSingleton;
import com.wireless.main.Packet;
import com.wireless.tables.NeighborTableItem.ConnectionType;

public class NeighborTable
{
  /*
   * This class contains a list of 1 hop neighbor nodes (nodes it can
   * receive from) and a list of 2 hop neighbor nodes.
   * 
   * It contains a list of the 2 hop nodes you can get from each 1 hop node
   * 
   * contains a list of the MPR's of the node - must be bidirectional
   * 
   * all entries have to have a holding period that remove them from the table
   * after they expire or require them to get updated
   * 
   * sequence number is updated for the most recent MPR's of the node
   * 
   * table also contains list of nodes that have selected this table as their MPR
   */
  
  int sequenceNumber = 0;
  NetworkConfiguration config;
  ArrayList<NeighborTableItem> neighborTableItemArray = new ArrayList<NeighborTableItem>();
  ObjectSingleton os;
  final int TIME_OUT_TIME = 5;
  
  public NeighborTable()
  {
    os = ObjectSingleton.getInstance();
    config = os.getNetworkConfiguration();
    if(config == null)
    {
      System.out.println("Network Configuration is null for neighbor table.");
      System.exit(1);
    }
  }
  
  public void addItemToTable(NetworkNode node, ConnectionType type, 
      int numberOfHops, NetworkNode previousNode, LinkedList<NetworkNode> list)
  {
    neighborTableItemArray.add(new NeighborTableItem(node,System.currentTimeMillis(),
	type,numberOfHops, previousNode, list));
  }
  
  public void updateItemTime(NeighborTableItem item)
  {
    item.setTime(System.currentTimeMillis());
  }
  
  public ArrayList<NetworkNode> getUniDirectionalNodes()
  {
    ArrayList<NetworkNode> uniDirectionalNodes = new ArrayList<NetworkNode>();
    for(int i = 0; i < neighborTableItemArray.size(); i++)
    {
      if(neighborTableItemArray.get(i).getConnectionType() == ConnectionType.UNI_DIRECTIONAL
	  && neighborTableItemArray.get(i).getNumberOfHops() == 1)
      {
	 uniDirectionalNodes.add(neighborTableItemArray.get(i).getNode());
      }
    }
    return uniDirectionalNodes;
  }
  
  public ArrayList<NetworkNode> getBiDirectionalNodes()
  {
    ArrayList<NetworkNode> biDirectionalNodes = new ArrayList<NetworkNode>();
    for(int i = 0; i < neighborTableItemArray.size(); i++)
    {
      if(neighborTableItemArray.get(i).getConnectionType() == ConnectionType.BI_DIRECTIONAL
	  && neighborTableItemArray.get(i).getNumberOfHops() == 1)
      {
	 biDirectionalNodes.add(neighborTableItemArray.get(i).getNode());
      }
    }
    return biDirectionalNodes;
  }
  
  
  /*
   * Returns a list of nodes that are 1 hop away from the current node
   */
  /*
   public ArrayList<NetworkNode> get1HopNodes()
   {
     ArrayList<NetworkNode> oneHopNodes = new ArrayList<NetworkNode>();
     for(int i = 0; i < neighborTableItemArray.size(); i++)
     {
       if(neighborTableItemArray.get(i).getNumberOfHops() == 1)
       {
	 oneHopNodes.add(neighborTableItemArray.get(i).getNode());
       }
     }
     return oneHopNodes;
   }
   */

   
   /*
    * Returns a list of nodes that are 2 hops away from the current node
    */
   public ArrayList<TwoHopStructure> get2HopNodes()
   {
     ArrayList<TwoHopStructure> twoHopNodes = new ArrayList<TwoHopStructure>();
     for(int i = 0; i < neighborTableItemArray.size(); i++)
     {
       if(neighborTableItemArray.get(i).getNumberOfHops() == 2)
       {
	 twoHopNodes.add(new TwoHopStructure(neighborTableItemArray.get(i).getPreviousNode(), neighborTableItemArray.get(i).getNode()));
       }
     }
     return twoHopNodes;
   }
   
   public ArrayList<NetworkNode> get2HopNodesOnly()
   {
     ArrayList<NetworkNode> twoHopNodes = new ArrayList<NetworkNode>();
     for(int i = 0; i < neighborTableItemArray.size(); i++)
     {
       if(neighborTableItemArray.get(i).getNumberOfHops() == 2)
       {
	 twoHopNodes.add(neighborTableItemArray.get(i).getNode());
       }
     }
     return twoHopNodes;
   }
   

   public LinkedList<NeighborTableItem> getItemsWithNode(NetworkNode node)
   {
     LinkedList<NeighborTableItem> list = new LinkedList<NeighborTableItem>();
     for(int i = 0; i < neighborTableItemArray.size(); i++)
     {
       if(neighborTableItemArray.get(i).getNode().equals(node))
       {
	 list.add(neighborTableItemArray.get(i));
       }
     }
     return list;
   }
   
   
   /**
    * Returns the MPRs of the current node 
    * @return
    */
    public ArrayList<MPRSequencePair> getMPRofNode()
    {
      ArrayList<MPRSequencePair> mprForThisNode = new ArrayList<MPRSequencePair>();
      for(int i = 0; i < neighborTableItemArray.size(); i++)
      {
        if(neighborTableItemArray.get(i).getConnectionType() == ConnectionType.MPR_FOR_THIS_NODE)
        {
 	 mprForThisNode.add(new MPRSequencePair(neighborTableItemArray.get(i).getNode(),sequenceNumber));
        }
      }
      return mprForThisNode;
    }
  
    
    public ArrayList<NetworkNode> getMPRnodeList()
    {
      ArrayList<NetworkNode> mpr = new ArrayList<NetworkNode>();
      for(int i = 0; i < neighborTableItemArray.size(); i++)
      {
	if(neighborTableItemArray.get(i).getConnectionType() == ConnectionType.MPR_FOR_THIS_NODE)
	{
	  mpr.add(neighborTableItemArray.get(i).getNode());
	}
      }
      return mpr;
    }

    public ArrayList<NetworkNode> get1HopNodes()
    {
      ArrayList<NetworkNode> oneHopNodes = new ArrayList<NetworkNode>();
      for(int i = 0; i < neighborTableItemArray.size(); i++)
      {
	if(neighborTableItemArray.get(i).getConnectionType() == ConnectionType.UNI_DIRECTIONAL
	    || neighborTableItemArray.get(i).getConnectionType() == ConnectionType.BI_DIRECTIONAL
	    || neighborTableItemArray.get(i).getConnectionType() == ConnectionType.MPR_FOR_THIS_NODE)
	{
	  oneHopNodes.add(neighborTableItemArray.get(i).getNode());
	}
      }
      return oneHopNodes;
    }
    
    
    
   /*
    * Returns a list of the mpr selectors and their sequence numbers
    */
   public ArrayList<MPRSequencePair> getMPRSelectors()
   {
     ArrayList<MPRSequencePair> nodesThatHaveSelectedThisNodeAsMPR = new ArrayList<MPRSequencePair>();
     for(int i = 0; i < neighborTableItemArray.size(); i++)
     {
       if(neighborTableItemArray.get(i).getConnectionType() == NeighborTableItem.ConnectionType.SELECTED_THIS_NODE_AS_MPR)
       {
	 nodesThatHaveSelectedThisNodeAsMPR.add(new MPRSequencePair(neighborTableItemArray.get(i).getNode(),neighborTableItemArray.get(i).getSequenceNumber()));
       }
     }
     return nodesThatHaveSelectedThisNodeAsMPR;
   }
   
   public NeighborTableItem getNeighborTableItem(NetworkNode node, ConnectionType type)
   {
     for(int i = 0; i < neighborTableItemArray.size(); i++)
     {
       if(neighborTableItemArray.get(i).getNode().equals(node) &&
	   neighborTableItemArray.get(i).getConnectionType() == type)
       {
	 return neighborTableItemArray.get(i);
       }
     }
     return null;
   }
   
   public void removeItem(NeighborTableItem item)
   {
     neighborTableItemArray.remove(item);
   }
   
   public void clearOldEntries()
   {
     for(int i = neighborTableItemArray.size(); i > -1; i--)
     {
       if(TIME_OUT_TIME * 1000 + neighborTableItemArray.get(i).getTime() < System.currentTimeMillis())
       {
	 neighborTableItemArray.remove(i);
       }
     }
   }
   
   public void selectMPRs()
   {
     ArrayList<NetworkNode> oldMprs = getMPRnodeList();
     for(int i = 0; i < oldMprs.size(); i++)
     {
       getNeighborTableItem(oldMprs.get(i), ConnectionType.MPR_FOR_THIS_NODE).setConnectionType(ConnectionType.BI_DIRECTIONAL);
     }
     ArrayList<NetworkNode> biDirectionalNodes = getBiDirectionalNodes();
     ArrayList<NetworkNode> accessableNodes = new ArrayList<NetworkNode>();
     accessableNodes.addAll(biDirectionalNodes);
     accessableNodes.addAll(get2HopNodesOnly());
     int i = biDirectionalNodes.size();
     NetworkNode temp, temp2;
     incrementSequenceNumber();
     while(accessableNodes.size() > 0 && i > -1)
     {
       temp = biDirectionalNodes.get(i); 
       i--;
       boolean containsAccessableNodeLink = false;
       ArrayList<Integer> links = temp.getLinks();
       if(accessableNodes.contains(temp))
       {
	 containsAccessableNodeLink = true;
       }
       else
       {
	 for(int j = 0; j < links.size(); j++)
         {
	   temp2 = config.getNodeBasedOnNumber(links.get(i));
	   if(accessableNodes.contains(temp2))
	   {
	     containsAccessableNodeLink = true;
	     break;
	   }
         }
       if(containsAccessableNodeLink)
       {
	 getNeighborTableItem(temp, ConnectionType.BI_DIRECTIONAL).setConnectionType(ConnectionType.MPR_FOR_THIS_NODE);
         if(accessableNodes.contains(temp))
         {
           accessableNodes.remove(temp);
         }
         for(int j = 0; j < links.size(); j++)
         {
	   temp2 = config.getNodeBasedOnNumber(links.get(i));
	   if(accessableNodes.contains(temp2))
	   {
	     accessableNodes.remove(temp2);
	   }
         }
       }
     }
   }
 } 
   
  /*
   * Handles incrementing the sequence number. Used after creating a packet.
   */
  private void incrementSequenceNumber()
  {
    sequenceNumber++;
    if (sequenceNumber == (new Packet()).getMaxSequenceNumber())
    {
      sequenceNumber = 0;
    }
  }
}
