package com.wireless.tables;

import java.util.ArrayList;

import org.w3c.dom.NodeList;

import com.wireless.main.NetworkConfiguration;
import com.wireless.main.NetworkNode;
import com.wireless.main.ObjectSingleton;
import com.wireless.main.PacketUtils;

public class HelloMessage
{
  /*
   * Need to be able to extract the HelloMessage attributes from bytes
   * 
   * Needs to send a message containing
   *   - list of addresses for valid bi-directional links
   *   - list of neighbors it hears a hello message from that are not 
   *      a bi-directional link
   *   - a list of multipoint relays
   * 
   */
  NeighborTable neighborTable;
  byte[] packet;
  int packetPointer = 0;
  ArrayList<NetworkNode> biDirectionalNodes;
  ArrayList<NetworkNode> uniDirectionalNodes;
  ArrayList<MPRSequencePair> mprsOfTheNode;
  NetworkNode nodeSendingMessage;
  final static int PACKET_IDENTIFIER = 25;
  final static int PACKET_IDENTIFIER_BYTE_SIZE = 2;
  final int NODE_COUNT_BYTE_SIZE = 2;
  final static int TYPE_HEADER_SIZE = 2;
  final int NODE_NUMBER_BYTE_SIZE = 2;
  final int SEQUENCE_NUMBER_BYTE_SIZE = 4;
  final int HEADER_LENGTH = PACKET_IDENTIFIER_BYTE_SIZE + 3 * NODE_COUNT_BYTE_SIZE + NODE_NUMBER_BYTE_SIZE;
  
  
  
  public HelloMessage(NeighborTable neighborTable, NetworkNode nodeSendingMessage)
  {
    this.neighborTable = neighborTable;
    this.nodeSendingMessage = nodeSendingMessage;
  }
  
  public void setNeighborTable(NeighborTable neighborTable)
  {
    this.neighborTable = neighborTable;
  }
  
  public NeighborTable getNeighborTable(NeighborTable neighborTable)
  {
    return neighborTable;
  }
  


  public ArrayList<MPRSequencePair> getMprsOfTheNode()
  {
    return mprsOfTheNode;
  }

  public void setMprsOfTheNode(ArrayList<MPRSequencePair> mprsOfTheNode)
  {
    this.mprsOfTheNode = mprsOfTheNode;
  }

  public NetworkNode getNodeSendingMessage()
  {
    return nodeSendingMessage;
  }

  public void setNodeSendingMessage(NetworkNode nodeSendingMessage)
  {
    this.nodeSendingMessage = nodeSendingMessage;
  }

  public byte[] makePacket()
  {
    biDirectionalNodes = neighborTable.getBiDirectionalNodes();
    uniDirectionalNodes = neighborTable.getUniDirectionalNodes();
    mprsOfTheNode = neighborTable.getMPRofNode();
    int packetLength = HEADER_LENGTH + biDirectionalNodes.size() * NODE_NUMBER_BYTE_SIZE + uniDirectionalNodes.size() * NODE_NUMBER_BYTE_SIZE
	+ (NODE_NUMBER_BYTE_SIZE + SEQUENCE_NUMBER_BYTE_SIZE) * mprsOfTheNode.size();
    packet = new byte[packetLength];
    //packet type part of header
    PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(PACKET_IDENTIFIER,PACKET_IDENTIFIER_BYTE_SIZE),packetPointer,packet);
    //PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(0,1),packetPointer,packet);
    //packet[0] = 20;
    //packet[1] = 0;
    //number of bidirectional links
    
    PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(biDirectionalNodes.size(),NODE_COUNT_BYTE_SIZE),packetPointer,packet);
    //packet[2] = (byte)oneHopNodes.size();
    //number of unidirectional links
    
    PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(uniDirectionalNodes.size(),NODE_COUNT_BYTE_SIZE),packetPointer,packet);
    //packet[3] = (byte)twoHopNodes.size();
    //number of mprs of the node
    
    PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(mprsOfTheNode.size(),NODE_COUNT_BYTE_SIZE),packetPointer,packet);
    //packet[4] = (byte)mprsOfTheNode.size();
    //packetPointer = 5;
    //copy into packet current node number
    PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(nodeSendingMessage.getNumber(), NODE_NUMBER_BYTE_SIZE), packetPointer, packet);
    addNodesToPacket(biDirectionalNodes);
    addNodesToPacket(uniDirectionalNodes);
    addMPRsToPacket(mprsOfTheNode);
    return packet;
  }
  
  private void addNodesToPacket(ArrayList<NetworkNode> nodeList)
  {
    for(int i = 0; i < nodeList.size(); i++)
    {
      PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(nodeList.get(i).getNumber(), NODE_NUMBER_BYTE_SIZE), packetPointer, packet);
    }
  }
  
  private void addMPRsToPacket(ArrayList<MPRSequencePair> mprsList)
  {
    for(int i = 0; i < mprsList.size(); i++)
    {
      PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(mprsList.get(i).getNode().getNumber(), NODE_NUMBER_BYTE_SIZE), packetPointer, packet);
      PacketUtils.copyIntoPacket(PacketUtils.intToByteArray(mprsList.get(i).getSequenceNumber(), SEQUENCE_NUMBER_BYTE_SIZE), packetPointer, packet);
    }
  }
  
  public void extractInformation(byte[] message)
  {
    packetPointer = 2;
    packet = message;
    int biDirectionalNodeCount = extractNodeCount();
    int uniDirectionalNodeCount = extractNodeCount();
    int mprNodeCount = extractNodeCount();
    nodeSendingMessage = extractNode();
    biDirectionalNodes = new ArrayList<NetworkNode>();
    for(int i = 0; i < biDirectionalNodeCount; i++)
    {
      biDirectionalNodes.add(extractNode());
    }
    uniDirectionalNodes = new ArrayList<NetworkNode>();
    for(int i = 0; i < uniDirectionalNodeCount; i++)
    {
      uniDirectionalNodes.add(extractNode());
    }
    mprsOfTheNode = new ArrayList<MPRSequencePair>();
    for(int i = 0; i < mprNodeCount; i++)
    {
      mprsOfTheNode.add(extractMPR());
    }
  }
  
  public ArrayList<NetworkNode> getBiDirectionalNodes()
  {
    return biDirectionalNodes;
  }

  public void setBiDirectionalNodes(ArrayList<NetworkNode> biDirectionalNodes)
  {
    this.biDirectionalNodes = biDirectionalNodes;
  }

  public ArrayList<NetworkNode> getUniDirectionalNodes()
  {
    return uniDirectionalNodes;
  }

  public void setUniDirectionalNodes(ArrayList<NetworkNode> uniDirectionalNodes)
  {
    this.uniDirectionalNodes = uniDirectionalNodes;
  }

  public static boolean isHelloMessage(byte[] message)
  {
    byte[] type = new byte[PACKET_IDENTIFIER_BYTE_SIZE]; 
    for(int i = 0; i < TYPE_HEADER_SIZE; i++)
    {
      type[i] = message[i];
    }
    return PacketUtils.byteArrayToInt(type) == PACKET_IDENTIFIER;
  }
  
  private int extractNodeCount()
  {
    byte[] count = new byte[NODE_COUNT_BYTE_SIZE];
    for(int i = 0; i < NODE_COUNT_BYTE_SIZE; i++)
    {
      count[i] = packet[packetPointer];
      packetPointer++;
    }
    return PacketUtils.byteArrayToInt(count);
  }
  
  private NetworkNode extractNode()
  {
    byte[] nodeNumber = new byte[NODE_NUMBER_BYTE_SIZE];
    for(int i = 0; i < NODE_NUMBER_BYTE_SIZE; i++)
    {
      nodeNumber[i] = packet[packetPointer];
      packetPointer++;
    }
    return ObjectSingleton.getInstance().getNetworkConfiguration().getNodeBasedOnNumber(
	PacketUtils.byteArrayToInt(nodeNumber));
  }
  
  private MPRSequencePair extractMPR()
  {
    return new MPRSequencePair(extractNode(), extractSequenceNumber());
  }
  
  private int extractSequenceNumber()
  {
    byte[] sequenceNumber = new byte[NODE_NUMBER_BYTE_SIZE];
    for(int i = 0; i < NODE_NUMBER_BYTE_SIZE; i++)
    {
      sequenceNumber[i] = packet[packetPointer];
      packetPointer++;
    }
    return PacketUtils.byteArrayToInt(sequenceNumber);
  }
  
  public ArrayList<NetworkNode> getMPRnodeList()
  {
    ArrayList<NetworkNode> mprNodeList = new ArrayList<NetworkNode>();
    for(int i = 0 ; i < mprsOfTheNode.size(); i++)
    {
      mprNodeList.add(mprsOfTheNode.get(i).getNode());
    }
    return mprNodeList;
  }
  
}
