package com.wireless.tables;

import com.wireless.main.NetworkNode;

public class MPRSequencePair
{
  NetworkNode node;
  int sequenceNumber;
  
  public MPRSequencePair(NetworkNode node, int sequenceNumber)
  {
    this.node = node;
    this.sequenceNumber = sequenceNumber;
  }

  public NetworkNode getNode()
  {
    return node;
  }

  public void setNode(NetworkNode node)
  {
    this.node = node;
  }

  public int getSequenceNumber()
  {
    return sequenceNumber;
  }

  public void setSequenceNumber(int sequenceNumber)
  {
    this.sequenceNumber = sequenceNumber;
  }
}
