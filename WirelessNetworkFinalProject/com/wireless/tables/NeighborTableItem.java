package com.wireless.tables;

import java.util.LinkedList;

import com.wireless.main.NetworkNode;

public class NeighborTableItem
{
  
  public static enum ConnectionType 
  {
    TWO_HOP,
    UNI_DIRECTIONAL,
    BI_DIRECTIONAL,
    MPR_FOR_THIS_NODE,
    SELECTED_THIS_NODE_AS_MPR
  }
  long time;
  ConnectionType connectionType;
  NetworkNode node;
  int sequenceNumber;
  int numberOfHops;
  NetworkNode previousNode = null;
  int references = 0;
  
  public int getReferences()
  {
    return references;
  }


  public void setReferences(int references)
  {
    this.references = references;
  }


  public LinkedList<NetworkNode> getLinks()
  {
    return links;
  }


  public void setLinks(LinkedList<NetworkNode> links)
  {
    this.links = links;
  }

  LinkedList<NetworkNode> links = new LinkedList<NetworkNode>();
  
  public NetworkNode getPreviousNode()
  {
    return previousNode;
  }


  public void setPreviousNode(NetworkNode previousNode)
  {
    this.previousNode = previousNode;
  }


  public NeighborTableItem(NetworkNode node, long time, ConnectionType type,
      int numberOfHops, NetworkNode previousNode, LinkedList<NetworkNode> links)
  {
    this.node = node;
    this.time = time;
    this.connectionType = type;
    this.numberOfHops = numberOfHops;
    this.previousNode = previousNode;
    this.links = links;
    this.references = 1;
  }
  
  
  public void makeItemFromNode(NetworkNode node, long time, ConnectionType type,
      int numberOfHops, LinkedList<NetworkNode> links, int sequenceNumber)
  {
    this.node = node;
    this.time = time;
    this.connectionType = type;
    this.numberOfHops = numberOfHops;
    this.links = links;
    this.sequenceNumber = sequenceNumber;
    this.references = 1;
  }
  
  public NetworkNode getNode()
  {
    return node;
  }

  public void setNode(NetworkNode node)
  {
    this.node = node;
  }

  public int getNumberOfHops()
  {
    return numberOfHops;
  }

  public void setNumberOfHops(int numberOfHops)
  {
    this.numberOfHops = numberOfHops;
  }
  
  public long getTime()
  {
    return time;
  }

  public void setTime(long time)
  {
    this.time = time;
  }

  public ConnectionType getConnectionType()
  {
    return connectionType;
  }

  public void setConnectionType(ConnectionType connectionType)
  {
    this.connectionType = connectionType;
  }

  public int getSequenceNumber()
  {
    return sequenceNumber;
  }

  public void setSequenceNumber(int sequenceNumber)
  {
    this.sequenceNumber = sequenceNumber;
  }
  
  @Override
  public boolean equals(Object ntItem)
  {
    if(ntItem instanceof NeighborTableItem)
    {
      return (this.getNode().equals(((NeighborTableItem)ntItem).getNode()) &&
	  this.getConnectionType() == ((NeighborTableItem)ntItem).getConnectionType());
    }
    else
    {
      return false;
    }
    
  }
  
  @Override
  public int hashCode()
  {
    return (this.getNode().hashCode() * 1000 + this.getConnectionType().hashCode());
  }
  
  
}
