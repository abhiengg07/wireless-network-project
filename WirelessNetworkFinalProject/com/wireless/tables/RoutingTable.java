package com.wireless.tables;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import com.wireless.main.NetworkNode;

public class RoutingTable
{
  ArrayList<RoutingTableItem> routingArray = new ArrayList<RoutingTableItem>();
  NetworkNode currentNode;
  NeighborTable neighborTable;
  TopologyTable topologyTable;
  
  
  public RoutingTable(NetworkNode currentNode, NeighborTable neighborTable,
      TopologyTable topologyTable)
  {
    this.currentNode = currentNode;
    this.neighborTable = neighborTable;
    this.topologyTable = topologyTable;
  }
  
  public void update()
  {
    routingArray.clear();
    ArrayList<NetworkNode> oneHopNodes = neighborTable.get1HopNodes();
    NetworkNode tempNode;
    double tempDistance;
    for(int i = 0; i < oneHopNodes.size(); i++)
    {
      tempNode = oneHopNodes.get(i);
      //tempDistance = currentNode.calculateDistance(tempNode);
      routingArray.add(new RoutingTableItem(tempNode, currentNode, 1));
    }
    ArrayList<TwoHopStructure> twoHopNodes = neighborTable.get2HopNodes();
    TwoHopStructure tempStructure;
    for(int i = 0; i < twoHopNodes.size(); i++)
    {
      tempStructure = twoHopNodes.get(i);
      //tempDistance = currentNode.calculateDistance(tempStructure.getFirstHopNode());
      //tempDistance += tempStructure.getFirstHopNode().calculateDistance(tempStructure.getSecondHopNode());
      routingArray.add(new RoutingTableItem(tempStructure.getFirstHopNode(), tempStructure.getSecondHopNode(), 2));
    }
    ArrayList<Integer> hopsForNodes = new ArrayList<Integer>();
    ArrayList<TopologyItem> topologyArray = topologyTable.getTopologyArray();
    TopologyItem tempTopItem;
    RoutingTableItem routingItem;
    for(int i = 0; i < topologyArray.size(); i++)
    {
      tempTopItem = topologyArray.get(i);
      routingItem = calculateHops(new RoutingTableItem(tempTopItem.getDestination(),
	  tempTopItem.getDestination(), 0));
      if(routingItem != null)
      {
        routingArray.add(routingItem);
      }
    }
  }
  
  private RoutingTableItem calculateHops(RoutingTableItem rti)
  {
    //at the moment I think I can get into an infinite loop 
    RoutingTableItem result = null;
    ArrayList<TopologyItem> topologyArray = topologyTable.getTopologyArray();
    TopologyItem tempTopItem;
    ArrayList<NetworkNode> oneHopNodes = neighborTable.get1HopNodes();
    Queue<RoutingTableItem> queue = new LinkedList<RoutingTableItem>();
    queue.add(rti);
    RoutingTableItem currentRoutingTableItem;
    while(!queue.isEmpty())
    {
      currentRoutingTableItem = queue.remove();
      for(int i = 0; i < topologyArray.size(); i++)
      {
         tempTopItem = topologyArray.get(i);
         if(tempTopItem.getDestination().equals(currentRoutingTableItem.getNextHop()))
         {
           if(oneHopNodes.contains(tempTopItem.getLastHop()))
           {
	     currentRoutingTableItem.setNextHop(tempTopItem.getLastHop());
	     currentRoutingTableItem.setDistance(currentRoutingTableItem.getDistance() + 2);
	     return currentRoutingTableItem;
           }
           else
           {
	     queue.add(new RoutingTableItem(currentRoutingTableItem.getDestination(),
		 tempTopItem.getLastHop(), currentRoutingTableItem.getDistance() + 1));
           }
         }
      }
      
    }
    return null; //not found
  }
  
  
  public RoutingTableItem getRouteToDestination(NetworkNode destination)
  {
    for(int i = 0; i < routingArray.size(); i++)
    {
      if(routingArray.get(i).getDestination().equals(destination))
      {
	return routingArray.get(i);
      }
    }
    return null;
  }

  public ArrayList<RoutingTableItem> getRoutingArray()
  {
    return routingArray;
  }
  
  public void printToFile()
  {
    String fileContent = "Destination\t\tNext Hop\t\tNumber of Hops\n";
    RoutingTableItem item;
    for(int i = 0; i < routingArray.size(); i++)
    {
      item = routingArray.get(i);
      fileContent += item.destination.getNumber()+"\t\t"+item.getNextHop().getNumber()+"\t\t"+item.getDistance()+"\n";
    }
    File file = new File("RoutingTableForNode"+currentNode.getNumber()+".txt");
    FileWriter fw;
    try
    {
      fw = new FileWriter(file.getAbsoluteFile());
      BufferedWriter bw = new BufferedWriter(fw);
      try
      {
        bw.write(fileContent);
      } catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      try
      {
        bw.close();
      } catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    
  }
  
}
