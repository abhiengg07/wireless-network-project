package com.wireless.tables;

import com.wireless.main.NetworkNode;

public class TwoHopStructure
{
  
  NetworkNode firstHopNode;
  NetworkNode secondHopNode;

  public TwoHopStructure(NetworkNode firstHopNode, NetworkNode secondHopNode)
  {
    this.firstHopNode = firstHopNode;
    this.secondHopNode = secondHopNode;
  }

  public NetworkNode getFirstHopNode()
  {
    return firstHopNode;
  }

  public void setFirstHopNode(NetworkNode firstHopNode)
  {
    this.firstHopNode = firstHopNode;
  }

  public NetworkNode getSecondHopNode()
  {
    return secondHopNode;
  }

  public void setSecondHopNode(NetworkNode secondHopNode)
  {
    this.secondHopNode = secondHopNode;
  }
  
  @Override public boolean equals(Object o)
  {
    if(o instanceof TwoHopStructure)
    {
      if(((TwoHopStructure)o).getFirstHopNode().equals(this.getFirstHopNode()) &&
	  ((TwoHopStructure)o).getSecondHopNode().equals(this.getSecondHopNode()))
      {
	return true;
      }
      else
      {
	return false;
      }
    }
    else
    {
      return false;
    }
  }
  
  @Override
  public int hashCode()
  {
    return this.getFirstHopNode().hashCode() * 4000 + this.getSecondHopNode().hashCode();
  }
  

}
