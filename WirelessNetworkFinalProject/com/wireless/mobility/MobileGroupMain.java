package com.wireless.mobility;

import java.util.Scanner;

public class MobileGroupMain
{

  final static int SECONDS_TO_WAIT = 1;
  
  /**
   * @param args
   */
  public static void main(String[] args)
  {
    Scanner userInput = new Scanner(System.in);
    System.out.println("Please input a configuration file.");
    String configFileName = userInput.next();
    userInput.close();
    MobileGroup mg = new MobileGroup(configFileName);
    MobileNodesGui gui = new MobileNodesGui();
    gui.buildGUI(mg.getArea());

    long lastTime = System.currentTimeMillis();

    while (true)
    {
      if (lastTime + SECONDS_TO_WAIT * 1000 < System.currentTimeMillis())
      {
	mg.update(gui);
	lastTime = System.currentTimeMillis();
      }
    }

  }

}
