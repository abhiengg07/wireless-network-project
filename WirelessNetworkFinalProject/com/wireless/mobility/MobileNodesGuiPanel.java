package com.wireless.mobility;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.plaf.metal.MetalBorders.Flush3DBorder;

import com.wireless.main.NetworkNode;

public class MobileNodesGuiPanel extends JPanel
{
  
  /**
   * 
   */
  private static final long serialVersionUID = 70L;
  final int EXTRA_WINDOW_AREA = 100;
  MobileArea area;
  BufferedImage image = null;
  final int NODE_SIZE = 15;
  final int GRID_OFFSET = EXTRA_WINDOW_AREA / 2;
  Graphics imageG = null;
  final int SCALE = 2;
  
  public MobileNodesGuiPanel(MobileArea area)
  {
    this.area = area;
    setPreferredSize(new Dimension(area.getHeight() * SCALE + EXTRA_WINDOW_AREA,area.getWidth() * SCALE + EXTRA_WINDOW_AREA));
    image = new BufferedImage(area.getHeight() * SCALE + EXTRA_WINDOW_AREA, 
	area.getWidth() * SCALE + EXTRA_WINDOW_AREA, BufferedImage.TYPE_3BYTE_BGR);
  }
  
  public void drawGrid()
  {
    if(image != null)
    {
      update(getGraphics());
    }
    //frame.setSize();
    //panel.paintComponents(g);
    if(imageG != null)
    {
      imageG.dispose();
     }
    image.flush();
    //Graphics imageG = image.getGraphics();
    imageG = image.getGraphics();
    ((Graphics2D)imageG).setPaint(Color.BLACK);
    ((Graphics2D)imageG).fillRect(0, 0, image.getWidth(), image.getHeight());
    ((Graphics2D)imageG).setPaint(Color.WHITE);
    ((Graphics2D)imageG).drawRect(GRID_OFFSET, GRID_OFFSET, area.getHeight() * SCALE, area.getWidth() * SCALE);
    ((Graphics2D)imageG).drawString("0", GRID_OFFSET - 20, GRID_OFFSET - 10);
    int amount = 0;
    int steps = 25;
    for(int i = 0; i < image.getHeight() * SCALE; i+= steps * SCALE )
    {
      if(i != 0)
      {
        ((Graphics2D)imageG).drawString(new Integer(amount).toString(), GRID_OFFSET - 20, GRID_OFFSET + i);
      }
      amount += steps;
    }
    amount = 0;
    for(int i = 0; i < image.getHeight() * SCALE; i+= steps * SCALE )
    {
      if(i != 0)
      {
        ((Graphics2D)imageG).drawString(new Integer(amount).toString(), GRID_OFFSET + i, GRID_OFFSET - 10);
      }
      amount += steps;
    }
  }
  
  public void drawNode(NetworkNode node)
  {
    //Graphics imageG = image.getGraphics();
    ((Graphics2D)imageG).setPaint(Color.YELLOW);
    ((Graphics2D)imageG).drawString(new Integer(node.getNumber()).toString(), new Double(node.getXCoordinate() * SCALE - NODE_SIZE / 4).intValue() + GRID_OFFSET, new Double(node.getYCoordinate() * SCALE + NODE_SIZE / 3).intValue() + GRID_OFFSET);
    ((Graphics2D)imageG).setPaint(Color.BLUE);
    ((Graphics2D)imageG).drawOval(new Double(node.getXCoordinate() * SCALE - NODE_SIZE / 2).intValue() + GRID_OFFSET, new Double(node.getYCoordinate() * SCALE - NODE_SIZE / 2).intValue() + GRID_OFFSET, NODE_SIZE, NODE_SIZE);
    
    //imageG.dispose();
  }
  
  @Override
  protected void paintComponent(Graphics g)
  {
    Graphics g2 = g.create();
    super.paintComponent(g2);
    //g2.clearRect(0, 0, image.getWidth(), image.getHeight());
    g2.drawImage(image, 0, 0, null);
    
    g2.finalize();
  }
}
