package com.wireless.mobility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.wireless.main.NetworkConfiguration;
import com.wireless.main.NetworkNode;
import com.wireless.main.ObjectSingleton;

public class MobileGroup
{

  Random rand = new Random();
  ArrayList<NetworkNode> nodes = new ArrayList<NetworkNode>();
  MobileArea area;
  NetworkConfiguration nc;
  ArrayList<NetworkNode> movingNodes;

  public MobileArea getArea()
  {
    return area;
  }

  public void setArea(MobileArea area)
  {
    this.area = area;
  }

  public ArrayList<NetworkNode> getNodes()
  {
    return nodes;
  }

  public void setNodes(ArrayList<NetworkNode> nodes)
  {
    this.nodes = nodes;
  }

  public MobileGroup(String filename)
  {
    nc = new NetworkConfiguration();
    // oSing.setNetworkConfiguration(nc);
    try
    {
      nc.readConfigurationFile(filename);
    } catch (IOException e1)
    {
      // TODO Auto-generated catch block
      e1.printStackTrace();
      System.exit(1);
    }
    this.area = new MobileArea(300);
    movingNodes = nc.getNodes();

    for (int i = 0; i < movingNodes.size(); i++)
    {
      int direction = rand.nextInt(360);
      movingNodes.get(i).setDirection(direction);
      movingNodes.get(i).setVelocity(rand.nextDouble() * 0.9 + 0.1);
      movingNodes.get(i).setArea(this.area);
    }

    // System.out.println(direction);
  }

  public void update(MobileNodesGui gui)
  {
    gui.drawGrid();
    for (int i = 0; i < movingNodes.size(); i++)
    {
      movingNodes.get(i).moveInArea();
      gui.drawNode(movingNodes.get(i));
    }
    nc.writeToConfigurationToFile();
  }

  public String toString()
  {
    NetworkNode tempNode;
    String result = "Nodes at time: " + System.currentTimeMillis() + "\n";
    for (int i = 0; i < nodes.size(); i++)
    {
      tempNode = nodes.get(i);
      result += "Node " + i + ": x pos: " + tempNode.getXCoordinate()
	  + " y pos: " + tempNode.getYCoordinate() + "\n";
    }
    return result;
  }
}
