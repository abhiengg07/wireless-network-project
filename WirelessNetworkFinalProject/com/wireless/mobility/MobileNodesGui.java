package com.wireless.mobility;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.DebugGraphics;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.wireless.main.NetworkNode;

public class MobileNodesGui
{
  //Graphics2D drawer = new Graphics2D();
  
  
  final int WINDOW_SIZE = 500;
  //Graphics g;
  JFrame frame;
  MobileArea area;
  MobileNodesGuiPanel panel;
  
  public void buildGUI(MobileArea area)
  {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
  } catch (ClassNotFoundException ex) {
  } catch (InstantiationException ex) {
  } catch (IllegalAccessException ex) {
  } catch (UnsupportedLookAndFeelException ex) {
  }
    
    frame = new JFrame("Mobile Nodes");
    panel = new MobileNodesGuiPanel(area);
    //panel.setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION);
    frame.add(panel);
    //panel.setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //frame.setSize(WINDOW_SIZE, WINDOW_SIZE);
    
    
    frame.pack();
    frame.setVisible(true);
    //g = panel.getGraphics();

    this.area = area;
  }
  
  public void clearFrame()
  {
    //panel.update(g);
  }

  public void drawGrid()
  {
    panel.drawGrid();
  }
  
  public void drawNode(NetworkNode node)
  {
    //System.out.println("X position: "+node.getxPos());
    //System.out.println("Y position: "+node.getyPos());
    //System.out.println("G is equal to null: "+(g==null));
    //System.out.println(panel.isShowing());
    panel.drawNode(node);    
  }
  
}