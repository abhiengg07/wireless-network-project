package com.wireless.mobility;

import com.wireless.main.NetworkNode;

public class MobileArea
{
  
  int height;
  int width;
  
  public MobileArea(int dimensionsSize)
  {
     height = dimensionsSize;
     width = dimensionsSize;
  }
  
  public int getHeight()
  {
    return height;
  }

  public void setHeight(int height)
  {
    this.height = height;
  }

  public int getWidth()
  {
    return width;
  }

  public void setWidth(int width)
  {
    this.width = width;
  }

  public MobileArea(int newHeight, int newWidth)
  {
    height = newHeight;
    width = newWidth;
  }
  
  public void moveNode(NetworkNode node)
  {
    double xMoveDistance = node.getVelocity() * Math.cos(((double)node.getDirection())/180 * Math.PI);
    double yMoveDistance = node.getVelocity() * Math.sin(((double)node.getDirection())/180 * Math.PI);
    //System.out.println("Velocity: "+node.getVelocity());
    //System.out.println("Direction: "+node.getDirection());
    //System.out.println("Y Move Distance: "+yMoveDistance);
    //System.out.println("X Move Distance: "+xMoveDistance);
    double newXPos = node.getXCoordinate() + xMoveDistance;
    double newYPos = node.getYCoordinate() + yMoveDistance;
    node.setXCoordinate(newXPos);
    node.setYCoordinate(newYPos);
    //finish this part tomorrow
    boolean outOfBounds;
    do
    {
      outOfBounds = false;
      if(newXPos < 0)
      {
        node.setXCoordinate(-1 * newXPos);
        newXPos = -1 * newXPos;
        outOfBounds = true;
      }
      else if(newXPos > width)
      {
        node.setXCoordinate(2 * width - newXPos);
        newXPos = 2 * width - newXPos;
        outOfBounds = true;
      }
      else if(newYPos < 0)
      {
	node.setYCoordinate(-1 * newYPos);
	newYPos = -1 * newYPos;
	outOfBounds = true;
      }
      else if(newYPos > height)
      {
	node.setYCoordinate(2 * height - newYPos);
	newYPos = 2 * height - newYPos;
	outOfBounds = true;
      }
      
      if(outOfBounds)
      {
	node.setDirection(node.getDirection() + 90);
        //System.out.println("!!!!!!!!!");
        //System.out.println("Node "+node.getNodeNumber()+" is out of bounds");
      }        
    }while(outOfBounds);
  }
}
