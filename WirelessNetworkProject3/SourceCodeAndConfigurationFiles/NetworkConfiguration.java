import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

/*
 * The class that maintains the network's state
 */
public class NetworkConfiguration implements Serializable
{
    /**
	 *  The id for the class.  Used when the object is saved
	 *  and then read again to make sure that it was successful.
	 */
	private static final long serialVersionUID = 500L;
	
	/*
	 * The list of the nodes in the network
	 */
	ArrayList<NetworkNode> nodes = new ArrayList<NetworkNode>();
	
	/*
	 *  The default constructor for the class.
	 */
	public NetworkConfiguration() 
	{
		
	}
	
	/*
	 * The method used to transmit the network configuration.
	 * 
	 * We are not sure if we need this at the moment so right now it 
	 * is not used and only saved the network to a file.  We can change
	 * it later broadcast the network.
	 */
	public void transmitNetworkConfiguration()
	{
		try
		{
			File test = new File("test.ser");
			FileOutputStream fs = new FileOutputStream(test);
			ObjectOutputStream out = new ObjectOutputStream(fs);
			out.writeObject(this);
			out.close();
			fs.close();
			System.out.println("Created file in: "+test.getAbsolutePath());
		}
		catch(IOException i)
		{
			i.printStackTrace();
		}
	}
	
	/*
	 * The class used to create the current network state.
	 * 
	 * At the moment we are not sure if we need it so it is not called and
	 * restores the network from a file.  We can later change it to 
	 * make the network after receiving a transmission.
	 */
	public void receiveFromNetworkConnection()
	{
		try
	      {
	         FileInputStream fileIn = new FileInputStream("test.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         NetworkConfiguration temp = (NetworkConfiguration) in.readObject();
	         setNodes(temp.getNodes());
	         in.close();
	         fileIn.close();
	      }
		   catch(IOException i)
	      {
	         i.printStackTrace();
	      }
		   catch(ClassNotFoundException c)
	      {
	         System.out.println("NetworkConfiguration class not found");
	         c.printStackTrace();
	      }
	}
	
	/*
	 * The method used to read the configuration file and check that is 
	 * is formatted correctly.  If the file is not formatted correctly, 
	 * an IOException occurs.  This class sets up the scanner for the file
	 * and then calls a submethod.
	 * 
	 * filename - the name of the file with the configuration information.
	 */
	public void readConfigurationFile(String filename) throws IOException
	{
		Scanner scan;
		try {
			scan = new Scanner(new File(filename));
			readConfigurationInformation(scan);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * The class reads the configuration information from the file.  It checks
	 * to see if the file format is correct and throws an IOExcpetion if it is not.
	 * 
	 * scan - the scannner object for the file.
	 */
	public void readConfigurationInformation(Scanner scan) throws IOException
	{
		
		int nodeNumber, port, xCoordinate, yCoordinate;
		String name;
		ArrayList<Integer> links;
		byte[] b = new byte[4];
		//Pattern p = Pattern.compile("tux\\d\\d\\d,");
		nodes.clear();
		while(scan.hasNextLine())
		{
		  if(!scan.hasNext())
		  {
			  break;
		  }
		  if(!scan.hasNext("Node"))
		  {
			System.out.println("|"+scan.next()+"|");  
			throw new IOException("The word \"Node\" doesn't start the line.");
		  }
		  scan.next();
		  if(!scan.hasNextInt())
		  {
			System.out.println(scan.next());  
			throw new IOException("The node number isn't formatted correctly.");
		  }
		  else
		  {
			nodeNumber = scan.nextInt();
			/*if(!scan.hasNext(p))
			{
				System.out.println("|"+scan.next()+"|");
				throw new IOException("The tux computer name was not formatted correctly.");
			}*/
			if(!scan.hasNext())
			{
				//System.out.println("|"+scan.next()+"|");
				throw new IOException("The computer name was not formatted correctly.");
			}
			else
			{
				name = scan.next();
				name = name.replace(",", "");
				  if(!scan.hasNextInt())
				  {
					throw new IOException("Port number is incorrect.");
				  }
				  else
				  {
					port = scan.nextInt();
					if(!scan.hasNextInt())
					{
						throw new IOException("X coordinate is incorrect.");
					}
					else
					{
				        xCoordinate = scan.nextInt();
						if(!scan.hasNextInt())
						{
							throw new IOException("Y coordinate is incorrect.");
						}
						else
						{
						    yCoordinate = scan.nextInt();
						    if(!scan.hasNext("links"))
							{
						    	System.out.println("|"+scan.next()+"|");
								throw new IOException("The word \"links\" wasn't found in the expected place.");
							}
						    else
						    {
						    	scan.next();
						    	links = new ArrayList<Integer>();
						    	while(scan.hasNextInt())
						    	{
						    		links.add(scan.nextInt());
						    	}
						    	NetworkNode nn = new NetworkNode(nodeNumber, name,b, port, xCoordinate, 
						    			yCoordinate, links);
						    	nodes.add(nn);
						    	
						    	//scan.nextLine();
						    }
						}
					}
				  
				}
			}
		  }
		}
		scan.close();
		setIPAddressOfNodes();
	}
	
	/*
	 * Returns the list of nodes in the network.
	 */
    public ArrayList<NetworkNode> getNodes()
    {
    	return nodes;
    }
    
    /*
     * Sets the list of nodes in the network.
     */
    public void setNodes(ArrayList<NetworkNode> newNodes)
    {
    	nodes = newNodes;
    }
    
    /*
     * Returns the list of nodes on the computer. 
     * The name of the computer stored in the object singleton is
     * used to check if the nodes is on the computer.
     */
    public ArrayList<NetworkNode> getComputerNodeList()
    {

		ArrayList<NetworkNode> computerNodes = new ArrayList<NetworkNode>();
		for(int i = 0; i < nodes.size(); i++)
		{
			if(nodes.get(i).getName().equals(ObjectSingleton.getInstance().getComputerName()))
			{
				computerNodes.add(nodes.get(i));
			}
		}
		return computerNodes;
    }
    
    /*
     * Returns the byte array of the IP address from a string.
     * Throws and IOException if the IP address is not properly 
     * formatted.
     * 
     * ipString - the string holding to IP address.
     */
    private byte[] extractIPAddress(String ipString) throws IOException
    {
    	  byte[] b = new byte[4];
    	  int pos = ipString.indexOf(".");
		  for(int j = 0; j < 3; j++)
	  	  {
		    if(pos == -1)
		    {
		    	JOptionPane.showMessageDialog(null, "Please input a valid IP address.", "Error",
                  JOptionPane.ERROR_MESSAGE);
		  	 
		    }
		    else
		    {
			  String number = ipString.substring(0,pos);
			  int num;
			  try{
			    num = Integer.parseInt(number);
		 	  }
			  catch(Exception ex)
			  {
				  throw new IOException("Please input a valid IP address.");
				  
			  }
			  if(num > 255 || num < 0)
			  {
				  throw new IOException("Please input a valid IP address.");
			  }
			  b[j] = (byte)num;
			  ipString = ipString.substring(pos+1);
			  pos = ipString.indexOf(".");
		    }
		  }
		  int num;
		  try{
		    num = Integer.parseInt(ipString);
		  }
		  catch(Exception ex)
		  {
			  throw new IOException("Please input a valid IP address.");
		  }
		  if(num > 255 || num < 0)
		  {
			  throw new IOException("Please input a valid IP address.");
		  }
		  b[3] = (byte)num;
		  return b;
    }
    
    /*
     * Returns the specific node in the network with the specified node
     * number
     * 
     * number - the node number
     */
    public NetworkNode getNodeBasedOnNumber(int number)
    
    {
    	for(int i = 0; i < nodes.size(); i++)
    	{
    		if(nodes.get(i).getNumber() == number)
    		{
    			return nodes.get(i);
    		}
    	}
    	return null;
    }
    
    /*
     * Used to set the IP address of nodes after reading the node information 
     * from the file.
     */
    public void setIPAddressOfNodes()
    {
      for(int i = 0; i < nodes.size(); i++)
      {
    	  try {
			nodes.get(i).setIPAdress(InetAddress.getByName(nodes.get(i).getName()).getAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	  
      }
    }
}
