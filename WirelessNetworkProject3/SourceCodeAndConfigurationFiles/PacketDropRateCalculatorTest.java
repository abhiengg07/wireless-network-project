
public class PacketDropRateCalculatorTest
{
  public static void main(String[] args)
  {
    Packet p1 = new Packet(0, 1, 99, 2, 0);
    Packet p2 = new Packet(0, 2, 99, 2, 0);
    Packet p3 = new Packet(1, 1, 99, 2, 0);
    Packet p4 = new Packet(10, 1, 99, 2, 0);
    Packet p5 = new Packet(45, 1, 99, 2, 0);
    PacketDropRateCalculator pdrc = new PacketDropRateCalculator();
    System.out.println(pdrc.getAndUpdateTotalPacketsLost(p1));
    System.out.println(pdrc.getAndUpdateTotalPacketsLost(p2));
    System.out.println(pdrc.getAndUpdateTotalPacketsLost(p3));
    System.out.println(pdrc.getAndUpdateTotalPacketsLost(p4));
    System.out.println(pdrc.getAndUpdateTotalPacketsLost(p5));
  }

}
