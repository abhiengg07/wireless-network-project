import java.util.Arrays;

/*
 * The class for the packets
 */
public class Packet 
{
    // the bytes in the packet
	byte[] packet;
	// the information stored in the packets
	byte[] packetInformation;
	// the pointer to the last bit of information added to the packet
	int packetInformationPointer = 0;
	// the size of the packet
	int packetSize = 128;
	// the sequence number of the packets
	int sequenceNumber = 0;
	// the address of the place where the packet originated from
	int sourceAddress;
	// the address where the packet is ultimately going
	int destinationAddress;
	// the address of the node that sent the packet
	int lastLocation;
	// the bit used to determine if the packet transmits bits 
	//   - at this point this is not used 
	int usedToTransmitSoundByte;
	// the number of bytes of the sequence number
	int sequenceNumberBytes = 4;
	// the number of bytes of the source address
	int sourceAddressBytes = 2;
	// the number of bytes of the destination address
	int destinationAddressBytes = 2;
	// the number of bytes of the last location
	int lastLocationBytes = 2;
	// the used to transmit Sound byte number
	int usedToTransmitSoundBitBytes = 1;
	// the size of the header
	int headerBytes = sequenceNumberBytes + sourceAddressBytes + destinationAddressBytes
			+ lastLocationBytes + usedToTransmitSoundBitBytes;
	// the size of the packet for infromation (not header)
	int informationSize = packetSize - headerBytes;
	// the max sequence number stored in the packet
	int maxSequenceNumber = 65536; // this is actually one more than the max sequence number

	/*
	 * The default constructor for the class
	 */
	public Packet()
	{
	  packet = new byte[packetSize];
	}
	
	/*
	 * The packet constructor 
	 * 
	 * newSequenceNumber - the sequence number
	 * newSourceAddress - the source address
	 * newDestinationAddress - the destination address of the packet
	 * newLastLocation - the last location of the packet
	 * newUsedToTransmitSound - if the packet is transmitting sound or not
	 */
	public Packet(int newSequenceNumber, int newSourceAddress, int newDestinationAddress,
			int newLastLocation, int newUsedToTransmitSound) 
	{
		packet = new byte[packetSize];
		sequenceNumber = newSequenceNumber;
		sourceAddress = newSourceAddress;
		destinationAddress = newDestinationAddress;
		lastLocation = newLastLocation;
		usedToTransmitSoundByte = newUsedToTransmitSound;
		
		
	}
	
	/*
	 * creates the bytes in the packet based on the information
	 * in the object instance and the information array
	 * 
	 * information - the information to transmit in the project
	 * 
	 * LengthException - throws an error if the information is the
	 *    wrong length
	 */
	public byte[] createPacket(byte[] information) throws LengthException 
	{
		createHeader();
		createPacketData(information);
		return packet;
	}
	
	/*
	 * Creates the header and puts it in the packet information.
	 */
	public void createHeader()
	{
		if(sequenceNumber > maxSequenceNumber)
		{
			sequenceNumber = sequenceNumber % maxSequenceNumber;
		}
		copyIntoPacket(intToByteArray(sequenceNumber, sequenceNumberBytes));
		copyIntoPacket(intToByteArray(sourceAddress, sourceAddressBytes));
		copyIntoPacket(intToByteArray(destinationAddress,destinationAddressBytes));
		copyIntoPacket(intToByteArray(lastLocation, lastLocationBytes));
		copyIntoPacket(intToByteArray(usedToTransmitSoundByte, usedToTransmitSoundBitBytes));
		
		
	}
	
	/*
	 * copies the byte array source into the packet
	 * 
	 * source - the byte array to copy into the packet.
	 */
	private void copyIntoPacket(byte[] source)
	{
		for(int i = 0; i < source.length; i++)
		{
			packet[packetInformationPointer] = source[i];
			packetInformationPointer++;
		}
	}
	
	/*
	 * Puts the packet data into the packet.  It fills in any extra packet space with
	 * filler bytes.
	 * 
	 * information - the information to put in the packet
	 * LengthException - the exception raised when the information array is too large.
	 */
	public void createPacketData(byte[] information)throws LengthException
	{
		if (information.length > informationSize)
		{
			throw new LengthException();
		}
		else
		{
			copyIntoPacket(information);
			byte[] filler = new byte[informationSize - information.length];
			copyIntoPacket(filler);		
			packetInformation = information;
		}
	}
	
	/*
	 * Used to extract the information into a packet object when 
	 * the system receives a packet
	 * 
	 * newPacket - the byte array used to make a new packet.
	 */
	public void setPacket(byte[] newPacket)
	{
		packet = newPacket;
		//printPacketBytes(newPacket);
		//printPacketBytes(packet);
		setSequenceNumber(extractSequenceNumber());
		setSourceAddress(extractSourceAddress());
		setLastLocation(extractLastLocation());
		try {
			setInformation(extractInformation());
		} catch (LengthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("Packet set with sequence number: "+getSequenceNumber()+" source address: "+getSourceAddress()+" last location: "+getLastLocation());
	}
	
	/*
	 * returns the source address out of the packet
	 */
	public int extractSourceAddress()
	{
		byte[] sourceAddressArray = new byte[sourceAddressBytes];
		int j = 0;
		for(int i = sequenceNumberBytes; i < sequenceNumberBytes + sourceAddressBytes; i++)
		{
			sourceAddressArray[j] = packet[i];
			j++;
		}
		return  byteArrayToInt(sourceAddressArray);
	}
	
	/*
	 * returns the sequence number out of the packet
	 */
	public int extractSequenceNumber()
	{
		byte[] sequenceNumberArray = new byte[sequenceNumberBytes];
		int j = 0;
		for(int i = 0; i < sequenceNumberBytes; i++)
		{
			//System.out.println("i: "+i);
			//System.out.println("j: "+j);
			//System.out.println("packet[i]: "+packet[i]);
			//System.out.println("sequenceNumberArray[j]: "+sequenceNumberArray[j]);
			sequenceNumberArray[j] = packet[i];
			j++;
		}
		return byteArrayToInt(sequenceNumberArray);
	}
	
	/*
	 * returns the node that transmitted the packet out of the packet
	 */
	public int extractLastLocation()
	{
		byte[] lastLocationArray2 = new byte[lastLocationBytes];
		int j = 0;
		for(int i = sequenceNumberBytes + sourceAddressBytes + destinationAddressBytes; 
				i < lastLocationBytes +sequenceNumberBytes + sourceAddressBytes + destinationAddressBytes;
				i++)
		{
			lastLocationArray2[j] = packet[i];
			j++;
		}
		return byteArrayToInt(lastLocationArray2);
	}
	
	/*
	 * sets the source address of the packet
	 * 
	 * newSourceAddress - the new source address of the packet
	 */
	public void setSourceAddress(int newSourceAddress)
	{
		sourceAddress = newSourceAddress;
	}
	
	/*
	 * returns the source address of the packet
	 */
	public int getSourceAddress()
	{
		return sourceAddress;
	}
	
	/*
	 * sets the sequence number of the packet
	 * 
	 * newSequenceNumber - the new sequence number of the packet
	 */
	public void setSequenceNumber(int newSequenceNumber)
	{
		sequenceNumber = newSequenceNumber;
	}
	
	/*
	 * sets the last location of the packet
	 * 
	 * newLastLocation - the new last transmitting node's address
	 */
	public void setLastLocation(int newLastLocation)
	{
		lastLocation = newLastLocation;
	}
	
	/*
	 * returns the sequence number of the packet
	 */
	public int getSequenceNumber()
	{
		return sequenceNumber;
	}
	
	/*
	 * returns the max sequence number of the packets
	 */
	public int getMaxSequenceNumber()
	{
		return maxSequenceNumber;
	}
	
	/*
	 * returns the address of the last transmitted node
	 */
	public int getLastLocation()
	{
		return lastLocation;
	}
	
	/*
	 * returns the size of the packets
	 */
	public int getPacketSize()
	{
		return packetSize;
	}
	
    /*
     * returns the information in the packets
     */
	public byte[] getPacketBytes()
	{
		return packet;
	}
	
	/*
	 * sets the information in the packet.
	 * 
	 * newInformation - the new information for the packet
	 * LengthException - an exception thrown if the information length 
	 *   not correct
	 */
	public void setInformation(byte[] newInformation) throws LengthException
	{
		if(newInformation.length != informationSize)
		{
			throw new LengthException();
		}
		packetInformation = newInformation;
	}
	
	/*
	 * Returns a copy of the information in stored in the packet
	 */
	public byte[] extractInformation()
	{
		return Arrays.copyOfRange(packet, headerBytes, packetSize);
		
	}
	
	/*
	 * returns the information not in the header that 
	 * is in the packet
	 */
	public byte[] getInformation()
	{
		return packetInformation;
	}
	
	/*
	 * returns the size of the information
	 */
	public int getInformationSize()
	{
		return informationSize;
	}
	
	/*
	 * replaces the last location value
	 */
	public void setAndReplaceLastLocationInPacket(int newLastLocation)
	{
		setLastLocation(newLastLocation);
		byte[] lastLocationByteArray = intToByteArray(lastLocation, lastLocationBytes);
		int j = 0;
		int previousHeaderBytes = sequenceNumberBytes + sourceAddressBytes + destinationAddressBytes;
		for(int i = previousHeaderBytes; i < previousHeaderBytes + lastLocationBytes; i++)
		{
			packet[i] = lastLocationByteArray[j];
			j++;
		}
	}
	
	/*
	 * converts an integer value to a byte array
	 */
	public byte[] intToByteArray(int value, int size) {
	   byte[] b = new byte[size]; 
	   for(int i = 0; i < size; i++)
	   {
		b[i] = (byte)(value >> (8 * i));  
	   }
	   return b;
	}
	
	/*
	 * Converts byte arrays to ints
	 * 
	 */
	public int byteArrayToInt(byte[] b)
	{
		int result = 0;
		for(int i = 0; i < b.length; i++)
		{
			result = result | ((b[i] << (8*i)) & (0x000000FF << (8* i))); 
		}

		return result;
	}
	
	public void printPacketBytes(byte [] p)
	{
		System.out.print("Packet bytes: ");
		for(int i = 0; i < packet.length; i++)
		{
			System.out.print(p[i]+" ");
		}
		System.out.println();
	}
	

	

	
}
