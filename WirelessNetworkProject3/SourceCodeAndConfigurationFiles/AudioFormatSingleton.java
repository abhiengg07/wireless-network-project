import javax.sound.sampled.AudioFormat;

/*
 * The class that holds the audio format information.
 * And makes sure that the audio format remains constant for the multiple threads.
 */
public class AudioFormatSingleton {

	// The singleton object for the class
	private static AudioFormatSingleton instance = null;
	// The number of audio samples per second
	float samplesPerSecond = 8000;
	// The size of the samples in bits
	int sampleSizeInBits = 8;
	// The number of channels recording from (1 means non directed sound,
	//   2 means left and right sound channels.)
	int numberOfChannels = 1;
	// The boolean for if the sound range is signed
	boolean signed = true;
	// The boolean for bigEndian sound encoding
	boolean bigEndian = true;
	// The audio format object
	AudioFormat format = new AudioFormat(samplesPerSecond, sampleSizeInBits, numberOfChannels, signed, bigEndian);
	
	/*
	 * Private default object constructor
	 */
	private AudioFormatSingleton() 
	{
		
	}
	
	/*
	 * Returns the AudioFormatSingleton Instance.
	 * Insures that there is only ever one copy of the audio format
	 * singleton.
	 */
	public static AudioFormatSingleton getInstance()
	{
		if(instance == null)
		{
			instance = new AudioFormatSingleton();
			return instance;
		}
		else
		{
			return instance;
		}
	}
	
	/*
	 * Returns the AudioFormat object held by the AudioFormatSingleton
	 */
	public AudioFormat getFormat()
	{
		return format;
	}

}
