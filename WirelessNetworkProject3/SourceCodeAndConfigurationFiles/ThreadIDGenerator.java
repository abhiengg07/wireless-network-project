/*
 * This class is used to generate thread IDs
 */
public class ThreadIDGenerator {

	// the thread id count
	static int id = -1;
	
	/*
	 * increments the id count and returns the id;
	 */
	public  static int getID()
	{
		id++;
		return id;
	}
	

}
