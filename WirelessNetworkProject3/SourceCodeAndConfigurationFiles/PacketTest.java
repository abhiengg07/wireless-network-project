
public class PacketTest 
{

	public static void main(String[] args) 
	{
		Packet p = new Packet(23,2,65535,2,0);
		//p.setSequenceNumber(23);
		System.out.println("p's sequence number: "+p.getSequenceNumber());
		//p.setSourceAddress(2);
		System.out.println("p's source address: "+p.getSourceAddress());
		//p.setLastLocation(2);
		System.out.println("p's last location: "+p.getLastLocation());
		byte[] b = new byte[p.getInformationSize()];

		try {
			p.createPacket(b);
		} catch (LengthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print("p's bytes: ");
		p.printPacketBytes(p.getPacketBytes());
		Packet testPacket = new Packet();
		testPacket.setPacket(p.getPacketBytes());
		System.out.println("Sequence number: "+testPacket.extractSequenceNumber());
		System.out.println("Source Address: "+testPacket.extractSourceAddress());
		System.out.println("Last Location: "+testPacket.extractLastLocation());
	}
}
