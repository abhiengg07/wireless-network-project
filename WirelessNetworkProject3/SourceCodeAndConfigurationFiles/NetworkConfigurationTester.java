import java.io.IOException;
import java.util.ArrayList;

/*
 * This is just a test class. Not really part of the project.
 */
public class NetworkConfigurationTester {

	public NetworkConfigurationTester() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		test3();
		
	}
	
	public static void test1()
	{
	  NetworkConfiguration nc = new NetworkConfiguration();
	  try 
	  {
		  nc.readConfigurationFile("C:\\Users\\Zack\\Zack\'s Programs\\eclipse_workspace\\Wireless_Networks_Project2\\src\\testConfigurationFile.txt");
	  }
	  catch (IOException e) 
	  {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
      ArrayList<NetworkNode> nodes = nc.getNodes();
      for(int i = 0; i < nodes.size(); i++)
      {
    	System.out.println(nodes.get(i).toString());
      }
      nc.transmitNetworkConfiguration();
	}
	
	public static void test2()
	{
		NetworkConfiguration nc = new NetworkConfiguration();
		nc.receiveFromNetworkConnection();
		ArrayList<NetworkNode> nodes = nc.getNodes();
	    for(int i = 0; i < nodes.size(); i++)
	    {
	      System.out.println(nodes.get(i).toString());
	    }
		
	}
	
	public static void test3()
	{
		Thread thread1 = new Thread(new ConfigurationFileWatcher());
		thread1.start();
	}

}
