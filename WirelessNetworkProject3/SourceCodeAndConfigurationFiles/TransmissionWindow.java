import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/*
 * The class that handles the transmission windows.
 */
public class TransmissionWindow implements ActionListener{

	// the file change watcher for the configuration file
	ConfigurationFileWatcher cfw;
	// the reference to the file input frame
	JFrame fileFrame;
	// the reference to inputting the computer name frame
	JFrame computerNameFrame;
	// the frame for the transmission window.
	final JFrame transmissionWindowFrame = new JFrame();
	// the node number of the frame
	int nodeNumber;
	// the idStack for threads
	ThreadIDStack idStack = ThreadIDStack.getInstance();
	// the flooding protocol manager
	FloodingProtocolManager fpm;
	// the port number for the transmission window
	int port;
	
	/*
	 * the constructor for the TransmissionWindow class; it starts
	 * the receiving thread
	 * 
	 *  newCFw - the configuration file watcher
	 *  newFileFrame - the file watching frame
	 *  newComputerNameFrame - the computer name frame
	 *  newNodeNumber - the new number of the node
	 *  totalNodeCount - the count of the nodes in the network
	 *  newPort - the port for the transmission to recieve on
	 */
	public TransmissionWindow(ConfigurationFileWatcher newCFW, 
			JFrame newFileFrame, JFrame newComputerNameFrame, int newNodeNumber,
			int totalNodeCount, int newPort) 
	{
		cfw = newCFW;
		port = newPort;
		fileFrame = newFileFrame;
		computerNameFrame = newComputerNameFrame;
		nodeNumber = newNodeNumber;
		fpm = new FloodingProtocolManager(nodeNumber, nodeNumber, totalNodeCount);
		ObjectSingleton.getInstance().addToFPMHash(nodeNumber, fpm);
		int threadID = ThreadIDGenerator.getID();
		ThreadIDStack.getInstance().push(threadID);
		SoundTransmissionRunningManager.getInstance().addThread(threadID, true);
		Thread receivingThread = new Thread(new ReceiveAndSendThread(threadID, fpm,
				port));
		receivingThread.start();
	}
	
	/*
	 * creates the transmission window GUI
	 */
	public void makeWindow()
	{
		transmissionWindowFrame.setTitle("Flooding Algorithm Communicator - Recorder (Node "+new Integer(nodeNumber).toString()+")");
		JPanel frame2Panel = new JPanel();
		Container contentPane2 = transmissionWindowFrame.getContentPane();
		final JButton transmitButton = new JButton("Transmit");
		transmitButton.addMouseListener(new MouseAdapter()
		{
			//performs the actions required to stop and start the transmission
			@Override
            public void mouseClicked(MouseEvent e) 
			{
				//SoundTransmissionRunningManager strm = SoundTransmissionRunningManager.getInstance();
				if(transmitButton.getText().equals("Transmit"))
				{
					transmitButton.setText("Stop");
					ObjectSingleton.getInstance().setIsRecording(true);
					Thread rast = new Thread(new RecordAndSendThread(fpm));
					rast.start();
					
				}
				else
				{
					ObjectSingleton.getInstance().setIsRecording(false);
					transmitButton.setText("Transmit");
				}
			}
			
	    });
		final JButton newConfigurationFileButton = new JButton("New Configuration File");
		newConfigurationFileButton.addMouseListener(new MouseAdapter()
		{
			//handles the actions for new file button
			@Override
            public void mouseClicked(MouseEvent e) 
			{
				transmissionWindowFrame.setVisible(false);
				fileFrame.setVisible(true);
				cfw.setIsRunning(false);
				ArrayList<TransmissionWindow> twArray = wirelessproject2.getTransmissionWindows();
				for(int i = 0; i < twArray.size(); i++)
				{
					twArray.get(i).setInvisible();
				}
			}
		});
		
		final JButton newComputerNameButton = new JButton("New Computer Name");
		newComputerNameButton.addMouseListener(new MouseAdapter()
		{
			//handles the action for the new computer name button
			@Override
            public void mouseClicked(MouseEvent e) 
			{
				transmissionWindowFrame.setVisible(false);
				computerNameFrame.setVisible(true);
				ArrayList<TransmissionWindow> twArray = wirelessproject2.getTransmissionWindows();
				for(int i = 0; i < twArray.size(); i++)
				{
					twArray.get(i).setInvisible();
				}
			}
		});
		
			
		JRadioButton muteButton = new JRadioButton("Mute");
	    muteButton.setMnemonic(KeyEvent.VK_M);
	    muteButton.setActionCommand("Mute");
	    muteButton.setSelected(true);

	    JRadioButton playButton = new JRadioButton("Play");
	    playButton.setMnemonic(KeyEvent.VK_P);
	    playButton.setActionCommand("Play");

	    

	    //Group the radio buttons.
	    ButtonGroup group = new ButtonGroup();
	    group.add(muteButton);
	    group.add(playButton);

	    JPanel radioPanel = new JPanel(new GridLayout(0, 1));
        radioPanel.add(muteButton);
        radioPanel.add(playButton);
		
		frame2Panel.add(transmitButton);
		frame2Panel.add(newConfigurationFileButton);
		frame2Panel.add(newComputerNameButton);
		frame2Panel.add(radioPanel);
		
	    muteButton.addActionListener(this);
        playButton.addActionListener(this);
		
        
        
		contentPane2.add(frame2Panel);
		transmissionWindowFrame.pack();
		transmissionWindowFrame.setSize(600,200);
		transmissionWindowFrame.setVisible(true);
		transmissionWindowFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	}
	
	public void setInvisible()
	{
		transmissionWindowFrame.setVisible(false);
			SoundTransmissionRunningManager.getInstance()
			.removeThread(nodeNumber); // need to come back to and replace it with the recieve and send thread
			
	}
	
	public void actionPerformed(ActionEvent e) 
	{
        if(e.getActionCommand().equals("Mute"))
        {
        	ObjectSingleton.getInstance().setIsPlaying(false);
        	System.out.println("Muted");
        }
        else
        {
        	ObjectSingleton.getInstance().setIsPlaying(true);
        	System.out.println("Playing");
        }
    }
}
