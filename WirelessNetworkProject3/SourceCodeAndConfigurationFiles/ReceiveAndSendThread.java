import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;


/*
 * This thread is used to receive and send packets
 */
public class ReceiveAndSendThread implements Runnable {

	// the id of the thread
	int threadID;
	// the manager for the thread
	FloodingProtocolManager fpm;
	// the port for the thread
	int port;
	ObjectSingleton objectSingleton = ObjectSingleton.getInstance();
	PacketDropRateCalculator pdrc;
	/*
	 * the constructor for the thread
	 * 
	 * newThreadID - the thread id
	 * newFPM - the flooding protocol manager for the thread
	 * newPort - the port for the thread
	 */
	public ReceiveAndSendThread(int newThreadID, FloodingProtocolManager newFPM, 
			int newPort)
	{
		threadID = newThreadID;
		fpm = newFPM;
		port = newPort;
		pdrc = new PacketDropRateCalculator();
	}
	
	/*
	 * This method receives data from the specified port and
	 * then forwards it to the protocol manager which sends
	 * the packet to the node's links.  It also send the packet
	 * to the playThread to see if it will be played.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		DatagramSocket ds = null;
		DatagramSocket sender = null;
		try {
			ds = new DatagramSocket(port);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			sender = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		fpm.setSendingSocket(sender);
		AudioFormatSingleton afs = AudioFormatSingleton.getInstance();
        //playback audio
        int bufferSize = (int) afs.getFormat().getSampleRate() * afs.getFormat().getFrameSize();
        byte buffer[] = new byte[bufferSize];
        SoundTransmissionRunningManager strm = SoundTransmissionRunningManager.getInstance();
        DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);
        Packet packet = new Packet();
        SoundInformationLock sil = SoundInformationLock.getInstance();
        PlayThread pt = new PlayThread();
        //RecordAndSendThread rast = new RecordAndSendThread(fpm);
        boolean setUpPlaying = false;
       // boolean setUpRecording = false;
        try {
			ds.setSoTimeout(500);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			while((true))
			{
				//System.out.println("Thread waiting to recieve packet: "+threadID);
				try
				{
				  ds.receive(incoming);
				  packet.setPacket(incoming.getData());  //may have to make threads synchronize better later
				  System.out.println("Receveived packet: "+packet.getSequenceNumber()+" at port: "+port);
				  System.out.println("Total packets lost from node: "+packet.getLastLocation()+" is: "+
				  pdrc.getAndUpdateTotalPacketsLost(packet));
				  
				  if(ObjectSingleton.getInstance().getIsPlaying() && !setUpPlaying)
				  {
				  	pt.setUp();
				  	setUpPlaying = true;
				  }
				  else if(!ObjectSingleton.getInstance().getIsPlaying() && setUpPlaying)
				  {
				  	pt.tearDown();
				  	setUpPlaying = false;
				  }
				  if(fpm.checkAndSendPacket(packet) && setUpPlaying)
				  {
				  	pt.play(packet);
				  }
				}
				catch(SocketTimeoutException e)
				{
					
				}
		/*	  if(ObjectSingleton.getInstance().getIsRecording() && !setUpRecording)
			  {
				rast.setUpRecording();
				setUpRecording = true;
			  }
			  else if(!ObjectSingleton.getInstance().getIsRecording() && setUpRecording)
			  {
				rast.tearDownRecording();
				setUpRecording = false;
			  }
			  if(setUpRecording)
			  {
				rast.record();
			  }*/
			} 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ds.close();
        ds.disconnect();
        sender.close();
        sender.disconnect();

	}

}
