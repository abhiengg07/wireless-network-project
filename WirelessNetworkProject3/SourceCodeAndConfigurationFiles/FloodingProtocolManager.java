import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * The class that manages the flooding protocol.
 * The implementation of the flooding protocol is described
 * in this class
 */
public class FloodingProtocolManager {

	// the current node for which the manager is working
	int currentNode;
	// the address of the current node
	int selfAddress;
	// they number of bytes of the selfAddress
	int selfAddressBytes = 2;
	// the table that contains the last sequence number from
	// each node
	int[] cacheTable;
	// the currentSequenceNumber for the current node's transmissions
	int currentSeqeunceNumber = 0;
	// the destination address. At this time it is the broadcast address: 65535.
	int destinationAddress = 65535;
	// the socket to send the information to.
	DatagramSocket sender = null;
	
	/*
	 * Creates a flooding protocol manager project.  
	 * newCurrentNode - the current node for the manager
	 * newSelfAddresss - the address of the current node.
	 * numberOfNodes - the number of nodes in the system.
	 */
	public FloodingProtocolManager(int newCurrentNode, int newSelfAddress, int numberOfNodes) 
	{
		currentNode = newCurrentNode;
		selfAddress = newSelfAddress;
		cacheTable = new int[numberOfNodes];
		for(int i = 0; i < numberOfNodes; i++) 
		{
			cacheTable[i] = -1;
		}
	}

	/*
	 * Determines if the packet originated from the current source.
	 * Returns true if it did, and false if it did not.
	 * packet - the packet to check the source of.
	 */
	public boolean isFromSelf(Packet packet)
	{
		int packetAddress = packet.getSourceAddress();
		if(packetAddress != selfAddress)
		{
		  return false;
		}
		else
		{
		  return true;
		}
	}
	
	/*
	 * Determines if the packet is new and if so, updates the 
	 * cacheTable accordingly.  This should be called instead of 
	 * isNewPacket when wanting to update the table as well.
	 * 
	 * returns true if the packet is new and false otherwise
	 * 
	 * packet - the packet whose sequence number is checked
	 */
	public boolean checkSeqeunceNumberOfPacket(Packet packet)
	{
		boolean isNew = isNewPacket(packet);
		if(isNew)
		{
			int packetSequenceNumber = packet.getSequenceNumber();
			int packetSource = packet.getSourceAddress();
			cacheTable[packetSource] = packetSequenceNumber;
			
		}
		return isNew;
		
	}
	
	/*
	 * The method used to check new packet's received and forward them
	 * if they are new.
	 */
	public boolean checkAndSendPacket(Packet packet)
	{
        boolean isNew = checkSeqeunceNumberOfPacket(packet);
		if(isNew)
		{
		  //System.out.println("node "+currentNode+" received a packet from node " +
		  //		packet.getSourceAddress() + " with a sequence number of "+packet.getSequenceNumber()
		  //		+" and a last hop of "+packet.getLastLocation());
		  forwardPacket(packet);
		}
		return isNew;
	}
	
	/*
	 *  Checks to see if the manager has received the packet or not.
	 *  
	 *  returns true if the packet is new and false otherwise
	 *  
	 *  packet - the packet to check if new.
	 */
	public boolean isNewPacket(Packet packet)
	{
		int packetSequenceNumber = packet.getSequenceNumber();
		int packetSource = packet.getSourceAddress();
		boolean isNewPacket;
		if(!isFromSelf(packet))
		{
		  if(cacheTable[packetSource] > packet.getMaxSequenceNumber() - 100)
		  {
			if(packetSequenceNumber < 100 || cacheTable[packetSource] < packetSequenceNumber)
			{
				isNewPacket = true;
			}
			else
			{
				isNewPacket = false;
			}
	  	  }
		  else
		  {
		    if(cacheTable[packetSource] < packetSequenceNumber)
		    {
			  isNewPacket = true;
		    }
		    else
		    {
			  isNewPacket = false;
		    }
	 	  }
		}
		else
		{
			isNewPacket = false;
		}
		return isNewPacket;
	}
	
	/*
	 * The method that manages sending the packet to all of the 
	 * necessary linked nodes.
	 * 
	 * packet - the packet to send
	 */
	public void forwardPacket(Packet packet)
	{
		ArrayList<Integer> links = ObjectSingleton.getInstance().getNetworkConfiguration()
				.getNodeBasedOnNumber(currentNode).getLinks();
		String oldlinks = "";
		for(int i = 0; i < links.size(); i++)
		{
			oldlinks += links.get(i).toString()+ " ";
		}
		//System.out.println("Last location: "+packet.getLastLocation());
		if(links.contains(packet.getLastLocation()))
		{
			links.remove(links.indexOf(packet.getLastLocation()));
		}
		String newlinks = "";
		for(int i = 0; i < links.size(); i++)
		{
			newlinks += links.get(i).toString()+ " ";
		}
		//System.out.println("oldlinks: "+oldlinks+ " newlinks: "+newlinks+ " old last location: "+packet.getLastLocation()+" new last location: "+selfAddress);
		packet.setAndReplaceLastLocationInPacket(selfAddress);
		//System.out.println("packet last location: "+packet.getLastLocation());
		//System.out.println("packet last location in packet: "+packet.extractLastLocation());
		//packet.printPacketBytes(packet.getPacketBytes());
		for(int i = 0; i < links.size(); i++)
		{
			
			NetworkNode destinationNode = ObjectSingleton.getInstance().getNetworkConfiguration()
					.getNodeBasedOnNumber(links.get(i));
			PacketDropRateAlgorthim pdra = new PacketDropRateAlgorthim();
			NetworkNode selfNode = ObjectSingleton.getInstance().getNetworkConfiguration()
			.getNodeBasedOnNumber(currentNode);
			pdra.calculateDistance(selfNode.getXCoordinate(), selfNode.getYCoordinate(), 
					destinationNode.getXCoordinate(), destinationNode.getYCoordinate());
			if(!pdra.shouldDropPacket())
			{
                          System.out.println("Sending packet: "+packet.getSequenceNumber()+" to node: "+links.get(i));
			  transmitPacket(packet, destinationNode.getIPAddress(), destinationNode.getPort());
			}
                        else
			    {
				System.out.println("Droped packet: "+packet.getSequenceNumber()+" to node: "+links.get(i));
			    }
		}
	}
	
	/*
	 *  The top method for sending a new message.  It creates
	 *  the packet for the message and then sends out the packet
	 *  to the nodes links.
	 *  
	 *  message - the bytes to send as the information, does not 
	 *     include the header.
	 */
	public void sendNewMessage(byte[] message)
	{
		int packetInformationSize = (new Packet()).getInformationSize();
		int loopNumber = message.length/packetInformationSize;
		Packet packet;
		for(int i = 0; i < loopNumber + 1; i++)
		{
			packet = new Packet(currentSeqeunceNumber,selfAddress,
					destinationAddress,selfAddress,0);
			//System.out.println("Sending sequence number: "+currentSeqeunceNumber);
			incrementSequenceNumber();
			byte[] information;
			if(i == loopNumber)
			{
				information = Arrays.copyOfRange(message,i * packetInformationSize,message.length);
			}
			else
			{
				information = Arrays.copyOfRange(message, i * packetInformationSize, (i + 1) * packetInformationSize);
			}
			
			try {
				packet.createPacket(information);
			} catch (LengthException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("Packet sequence number: "+packet.getSequenceNumber());
			forwardPacket(packet);
			
		}
	}
	
	/*
	 * Sends packet to the specified IP Address and port using 
	 * UDP.
	 * 
	 * packet - the packet to send
	 * ipAddress - the IP address of the destination
	 * port - the port to send the packet to.
	 */
	public void transmitPacket(Packet packet, byte[] ipAddress, 
			int port)
	{
		
		InetAddress ia = null;
		try {
			ia = InetAddress.getByAddress(ipAddress);
		} catch (UnknownHostException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		/*
		System.out.println("IP address: ");
		for(int i = 0; i < ipAddress.length; i++)
		{
			System.out.println((short)ipAddress[i]+".");
		}*/
		//System.out.println("Sending packet: "+packet.getSequenceNumber()+" to port: "+port);
		
		DatagramPacket dp = new DatagramPacket(packet.getPacketBytes(),packet.getPacketSize(),ia,port);
		try {
			//System.out.println("Sending packet at the sender: "+packet.getSequenceNumber());
			sender.send(dp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * Handles incrementing the sequence number.  Used
	 * after creating a packet.
	 */
	private void incrementSequenceNumber()
	{
		currentSeqeunceNumber++;
		if(currentSeqeunceNumber == (new Packet()).getMaxSequenceNumber())
		{
			currentSeqeunceNumber = 0;
		}
	}
	
	/*
	 * Returns the socket that the manager is sending to.
	 */
	public DatagramSocket getSendingSocket()
	{
		return sender;
	}
	
    /*
     * Sets the sending datagram socket for the manager
     * 
     * newSocket - the new socket
     */
	public void setSendingSocket(DatagramSocket newSocket)
	{
		sender = newSocket;
	}
	
	/*
	 * Returns the current node that the manager is running on.
	 */
	public int getCurrentNode()
	{
		return currentNode;
	}
}

