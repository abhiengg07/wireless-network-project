
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/*
 * The thread used to play sound on the computer
 */
public class PlayThread 
{
    // the sound output line
	SourceDataLine sLine;
	// the id of the thread
	
	/*
	 * Creates a play thread object.
	 * 
	 * newThreadID - the id of the thread
	 * newFPM - the flooding protocol manager of the thread
	 */
	public PlayThread() 
	{
	}
	
	
	public void setUp()
	{
		AudioFormatSingleton afs = AudioFormatSingleton.getInstance();
		DataLine.Info info = new DataLine.Info(SourceDataLine.class,afs.getFormat());
		sLine = null;
        try 
        {
			sLine = (SourceDataLine)AudioSystem.getLine(info);
		    sLine.open(afs.getFormat());
        } 
        catch (LineUnavailableException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        sLine.start();

       
	}
	
	public void tearDown()
	{
		 sLine.drain();
	     sLine.close();
	}
	
	/*
	 * This method checks if the sound has been received before and 
	 * plays it if it has not.
	 */
	public void play(Packet packet)
	{
		//SoundInformationLock sil = SoundInformationLock.getInstance();
		//Packet packet = sil.getInformation();
		


		//System.out.println("Playing packet "+packet.getSequenceNumber());  
		sLine.write(packet.getInformation(),0,packet.getInformationSize());
		  
		
	}

}
