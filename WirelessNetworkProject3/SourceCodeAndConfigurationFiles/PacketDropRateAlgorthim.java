import java.util.Random;

/*
 * The class that holds the algorithm for calculating the packet drop rate.
 */
public class PacketDropRateAlgorthim {

	// the distane used in the calculation
	double distance;
	// the maxDistance of the algorithm
	int maxDistance = 120;
	// the proportion rate for the algorithm
	double proportion = ((double)9)/2;
	//the error threshold for the algorithm
	int errorThreshold = 50;
	//the y Intercept for our equation
	int yInterecept = -440;
	
	Random rand = new Random();
	
	/*
	 * the default constructor
	 */
	public PacketDropRateAlgorthim() 
	{
		
	}
	
	/*
	 * Calculated the distance between two points
	 * 
	 * x1 - the x position of the first point
	 * y1 - the y position of the first point
	 * x2 - the x position of the second point
	 * y2 - the y position of the second point
	 */
	public void calculateDistance(int x1, int y1, int x2, int y2)
	{
		distance = Math.sqrt(Math.pow(x1 - x2,2)+ Math.pow(y1 - y2, 2));
	}
	
	/*
	 * Returns true if algorithm calculates that the current distance
	 * is out of range. based on max distance or max error allowance
	 */
	public boolean isOutOfRange()
	{
	  if(distance > maxDistance)
	  {
		  return false;
	  }
	  else
	  {
		  if(calculateErrorRate() > errorThreshold)
		  {
			  return false;
		  }
		  else
		  {
			  return true;
		  }
	  }
	}

	/*
	 * Calculates the error rate.
	 */
	public double calculateErrorRate()
	{
		//System.out.println("Distance: "+distance);
		//System.out.println("Proportion: "+proportion);
		//System.out.println("YIntercept: "+yInterecept);
		double result = distance*proportion+yInterecept;
		if (result < 0)
		{
			return 0;
		}
		return result;
	}
	
	/*
	 * Sets the distance to the new distance
	 */
	public void setDistance(double newDistance)
	{
		distance = newDistance;
	}
	
	/*
	 * Returns if the packet should be dropped based on the packet drop rate algorithm
	 */
	public boolean shouldDropPacket()
	{
		double errorRate = calculateErrorRate();
		if(errorRate < 1)
		{
			return false;
		}
		if(errorRate > 100)
		{
			return true;
		}
		else
		{
                        //System.out.println("Erorr rate: "+errorRate);
			if(rand.nextInt(100)  < errorRate)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
